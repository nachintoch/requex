# REQUEX
Herramienta de extracción de Requerimientos de Software para analizar y mejorar
documentación existente.  

Requex toma un directorio con archivos de texto que representen los textos
completos de Tesis o Reportes de Servicio Social que describan la producción o
mantenimiento de un software; y que hayan sido producidos por estudiantes en
proyectos académicos. Analiza los textos de dichos archivos para extraer de
ellos Requerimientos de Software y entradas de glosario para posteriormente
permitirle a sus usuarios inspeccionar los Requerimientos y otras dimensiones
que conforman una Especificación de Requerimientos de Software (ERS); de acuerdo
a los estándares ISO/IEEE/IEC 29148:2018, 15288:2008, 15289:2019, Essence y
SWEBOK, entre otras recomendaciones para la producción de estos documentos.

El resultado de la extracción e inspección de Requerimientos es un documento ERS
en formato PDF, el cuál especifica de forma parcial el software descrito en los
documentos de entrada.  

## Acerca de Requex
Requex es una herramienta de Ingeniería de Software Asistida por Computadora
que incorpora aplicaciones de la Inteligencia Artificial con el fin de asistir
el mantenimiento de proyectos de software académicos; que por cualquier razón
tengan deficiencias en la documentación del software.  

Requex utiliza Procesamiento de Lenguaje Natural por medio de spaCy y gensim
para identificar patrones gramaticales que suelen presentarse en la descripción
de Requerimientos de Software. También identifica términos relevantes que se
puede esperar deban ser descritos en el glosario de la ERS en la que se
especifica el software descrito por los documentos que se analicen con Requex.  

El proceso de inspección de Requerimientos puede ayudar a los programadores,
diseñadores de software, líderes de proyecto y otros interesados entender los
Requerimientos tal como se describen en los documentos de entrada. Requex guía
al usuario para informar cuando el documento a producir está incompleto;
señalando las faltas de forma explícita, y solicitando los datos que conforman
los Requerimientos, así como las dimensiones de la ERS a producir.  

Requex fue desarrollado como parte del proyecto de titulación de maestría de
Manuel "Nachintoch" Castillo. Consulte [TESIS] para más información.
Universidad Nacional Autónoma de México, 2021

# Dependencias del proyecto
 - spacy
 - gensim
 - flake8
 - pytest
 - pyinstaller
 - fpdf2
 - bs4  

# Distribución
Los lanzamientos de Requex pueden identificarse con etiquetas de Git que
corresponden a números de versión formados por tres números enteros de la forma:
#.#.#  

Las plataformas soportadas son:
 - Microsoft Windows
 - GNU/Linux
 - Paquetes de Python

## Licencia
Requex es distribuido bajo **licencia GNU GPL 3.0**, lo que implica una
distribución abierta no monetizada. Requex y sus componentes no-terceros pueden
ser utilizados y modificados de forma libre; siempre y cuando no se incorporen
en algún producto propietario.

Para más información consulte: https://www.gnu.org/licenses/

# Desarrollo

## Uso de las herramientas de desarrollo

### pytest
pytest ha sido configurado en el archivo `requex/setup.cfg`. Para ejecutar las
pruebas contenidas en el directorio `requex/test/`, únicamente hace falta moverse
al directorio raíz de este proyecto `requex/`, activar el entorno virtual bajo
bajo el que está construido el proyecto y ejecutar el comando `$ pytest`

### flake8
flake8 ha sido configurado en el archivo `requex/setup.cfg`. Para ejecutar la
evaluación del código contenido en los directorios `requex/src/` y
`requex/test/`, únicamente es necesario moverse al directorio raíz de este
proyecto `requex/`, activar el entorno virtual bajo bajo el que está construido
el proyecto y ejecutar el comando `$ flake8`

### spaCy
Para poder ejecutar Requex, es necesario descargar el modelo del español que
se emplea con spaCy. Para ello, es necesario ejecutar el siguiente comando
tras haber activado el entorno virtual de Python 3 que contenga este
repositorio:
```sh
python -m spacy download es_core_news_md
```

## Compilación
El script `make.py` contiene la configuración necesaria para compilar el
proyecto en un solo archivo distribuible que depende de la plataforma en la que
sea utilizado (por lo que técnicamente Requex también soporta Mac OS X o
distribuciones de Linux basadas en BSD si lo compila en uno de estos sistemas).
Esto es posible gracias a la dependencia del proyecto
[pyinstaller](https://pyinstaller.readthedocs.io/).  

Para compilar el proyecto basta con ejecutar el script `make.py` tras haber
habilitado un entorno virtual de Python 3 que contenga este repositorio con
todas las dependencias del proyecto instaladas:  
```sh
$ python make.py
```

Esto resultará en la creación del archivo ejecutable `dist/Requex`
(o `dist\Requex.exe` al compilar bajo Windows).  

# Uso

## Ejecutando Requex

### Ejecutando como paquete de Python 3 importado en un script
Para ejecutar Requex como paquete de Python 3, deberá utilizar las siguientes
instrucciones:  
```python
import os
import requex

dir_corpus = os.sep.join(['ruta', 'al', 'corpus (dir)', 'o', 'archivo.txt'])
nombre_proyecto = 'Nombre del proyecto de software a analizar'
requex.iniciar_extraccion(dir_corpus, nombre_proyecto)
```  

### Ejecutando como paquete de Python 3 desde una línea de comandos
Para ejecutar Requex desde una línea de comandos, debe contar con este
repositorio en sus sistema, un entorno virtual de Python 3 y las dependencias
del proyecto instaladas en el entorno virtual.  

Requex soporta varios argumentos de consola para configurar rápidamente el
análisis. Puede consultar la ayuda con el siguiente comando:
```sh
python src/requex/__init__.py -h
```  

O bien, puede omitir cualquier bandera; la aplicación iniciará con una pantalla
de configuración que le permite preparar el análisis utilizando un entorno
gráfico.  

### Ejecutando un archivo ejecutable
Los ejecutables de Requex pueden ser ejecutados de la misma forma que utilizando
la línea de comandos, omitiendo el entorno de Python:  
```sh
Requex.exe -h
```  

O bien, utilizando un explorador de archivos localice el ejecutable e inícielo
usando la tecla Enter, doble-click o el equivalente en su sistema.  

**Tip** Asegúrese de tener permisos de ejecución del binario o script de Requex  
**Tip** Recuerde que la extensión .exe únicamente aparece en las compilaciones
para Microsoft Windows

## Herramientas incluidas para transformar documentos en texto plano
El directorio util/ contiene un script *textex* que extrae el contenido textual
de archivos en formato PDF, DOC y DOCX; y permite depositar el texto extraido
en un archivo nuevo o en la salida estándar; de forma que puede usarse en
una secuencia de comandos.  

En util/ también encontraremos el script *elimina_espacios_ruta*, el cuál
renombra un archivo o directorio; opcionalmente de forma recursiva, sustituyendo
los espacios en blanco con '\_'. *textex* no soporta archivos en una ruta que
incluye espacios, por lo que puede usar *elimina_espacios_ruta* para permitir
la extracción de textos. *Requex* únicamente puede extraer requerimientos de
archivos de texto plano (sin embargo, soporta archivos codificados con
Unicode).  

Puede utilizar estas herramientas en secuencia de la siguiente manera:  
```sh
$ util/elimina_espacios_ruta.sh res/raw/
$ util/textex.sh -o res/text res/raw/
```

# Iconografías por:
- Freepik --> https://www.freepik.com www.flaticon.com
