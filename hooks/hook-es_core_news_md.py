# taken from https://stackoverflow.com/a/60360900
# HOOK FILE FOR SPACY SPANISH MODELS
from PyInstaller.utils.hooks import collect_all
from PyInstaller.utils.hooks import collect_data_files

data = collect_all('es_core_news_md')

datas = data[0]
binaries = data[1]
hiddenimports = data[2]

datas += collect_data_files('es_core_news_md')
