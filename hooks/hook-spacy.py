# taken from https://stackoverflow.com/a/60360900
# HOOK FILE FOR SPACY
from PyInstaller.utils.hooks import collect_all

data = collect_all('spacy')

datas = data[0]
binaries = data[1]
hiddenimports = data[2]

hiddenimports += ['srsly.msgpack.util']
