#!/usr/bin/env python3

# This file is part of Requex.

# Requex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Requex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Requex.  If not, see <https://www.gnu.org/licenses/>.

import PyInstaller.__main__

PyInstaller.__main__.run([
    'src/requex/__init__.py',
    '--onefile',
    '--windowed',
    '--noconfirm',
    '--clean',
    '--name=Requex',
    '--additional-hooks-dir=hooks'
    ])
