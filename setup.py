# This file is part of Requex.

# Requex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Requex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Requex.  If not, see <https://www.gnu.org/licenses/>.

from setuptools import find_packages, setup

# https://stackoverflow.com/a/50156706
# install the package in the development mode (python setup.py develop OR
# pip install --editable .). This way, requex is correctly integrated for
# distribution

setup(
    package_dir={'': 'src'},
    packages=find_packages(where='src')
    )
