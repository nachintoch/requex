#!/usr/bin/env python3

# This file is part of Requex.

# Requex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Requex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Requex.  If not, see <https://www.gnu.org/licenses/>.

from customui import ToolTip

import getopt
import inspect
import tkinter as tk
import logging
import sys
import os

from tkinter import filedialog
from tkinter import messagebox
from tkinter import ttk

from requex.ayuda import ContenidoAyuda
from requex.extractor import MineroRequerimientos

from requex import exportador

from requex.iu import ConfigIU
from requex.iu import HiloCarga

from requex.modelos import EspecificacionRequerimientosSoftware
from requex.modelos import Requerimiento


logging.basicConfig(filename='requex.log', datefmt='%d-%m-%y %H:%M:%S',
                    level=logging.DEBUG)
logger = logging.getLogger(__name__)


class Requex:

    ESTADO_EXTRACCION_REQ = 0
    ESTADO_INICIO_ANALISIS = 1
    ESTADO_PANTALLA_PRINCIPAL = 2
    ESTADO_INSPECCION_REQ = 3
    ESTADO_INSPECCION_GLOSARIO = 4
    ESTADO_INSPECCION_RESPONSABLES = 5
    ESTADO_INSPECCION_REFERENCIAS = 6
    ESTADO_RECICLANDO_REQ = 7
    ESTADO_EXPORTANDO_ERS = 8
    ESTADO_TERMINADO = 9

    def __init__(self, nombre_proyecto, hilo_carga=None, pantalla_inicial=None):
        self.__especificacion_requerimientos =\
            EspecificacionRequerimientosSoftware()
        self.nombre_proyecto = nombre_proyecto
        self.__papelera_requerimientos = {}
        self.__eleccion_iu = None
        self.__relacion_indice_lista_id = None
        self.__botones_iu = {}
        self.__iu_activa = pantalla_inicial
        self.__iu_requerimientos = None
        self.__iu_glosario = None
        self.__iu_responsables_emision = None
        self.__iu_referencias = None
        self.__iu_papelera_requerimientos = None
        self.__estado = None
        self.__accion_cancelada = False
        self.__hilo_carga = hilo_carga
        self.__pantalla_inicial = pantalla_inicial

    @property
    def nombre_proyecto(self):
        return self.__nombre_proyecto

    @nombre_proyecto.setter
    def nombre_proyecto(self, nombre):
        if not nombre:
            raise AttributeError('El nombre del proyecto no puede ser vacío')
        if type(nombre) is not str:
            raise AttributeError('El nombre del proyecto debe ser una cadena')
        nombre = Requex.formatea_texto(nombre)
        self.__nombre_proyecto = nombre
        self.__especificacion_requerimientos.titulo = 'Especificación '\
            + 'de Requerimientos de Software del proyecto \''\
            + self.nombre_proyecto + '\''

    @property
    def estado(self):
        return self.__estado

    def extrae_analiza(self, dir_corpus):
        self.__estado = Requex.ESTADO_EXTRACCION_REQ
        minero = MineroRequerimientos()
        corpus = self.__cargar_corpus(dir_corpus)
        textos_corpus = [texto for texto in corpus.values()]
        requerimientos, glosario = minero.extraer_requerimientos(textos_corpus)
        del minero
        self.__especificacion_requerimientos.alcances = 'El presente documento'\
            + ' contiene los requerimientos que describen el proyecto de '\
            + 'software "' + self.nombre_proyecto + '". De acuerdo con '\
            + 'Sommerville (2011) y el estándar ISO/IEC/IEEE 29148:2018; una '\
            + 'Especificación de Requerimientos de Software describe un '\
            + 'producto de software, indicando las funciones y cualidades que '\
            + ' debe cumplir y las expectativas que se tienen de ellas. La '\
            + 'información contenida en este documento describen estos '\
            + 'aspectos de "' + self.nombre_proyecto + '"; pero salvo que se'\
            + ' ejemplifique explícitamente, no abarca mecanismos ni '\
            + 'implementaciones de sus funciones o cualidades.'
        self.__especificacion_requerimientos.version = '0.1'
        for ref in ContenidoAyuda.obtener_referencias_por_defecto():
            self.__especificacion_requerimientos.agregar_referencia(ref)
        for req in requerimientos:
            self.__especificacion_requerimientos.agregar_requerimiento(req)
        for term in glosario:
            if term not in self.__especificacion_requerimientos.glosario:
                self.__especificacion_requerimientos.glosario.agregar_entrada(
                    term)
        self.__siguiente_id_requerimiento = len(requerimientos)
        self.__hilo_carga.terminar()
        self.__ventana_principal = self.__prepara_ventana_analisis(
            ContenidoAyuda.obtener_nombre_descripcion())
        self.__estado = Requex.ESTADO_INICIO_ANALISIS
        self.__configura_ventana_principal()

    def entrenar(self, dir_corpus, congelar_modelos=False):
        pass

    def verificar_completo(self):
        return self.__especificacion_requerimientos.verificar_completo()

    def __cargar_corpus(self, dir_origen):
        corpus = {}
        if os.path.isfile(dir_origen):
            with open(dir_origen, 'r') as f:
                corpus[dir_origen] = f.read()
        else:
            for documento in os.listdir(dir_origen):
                ruta = os.sep.join([dir_origen, documento])
                corpus.update(self.__cargar_corpus(ruta))
        return corpus

    def __registrar_exportadores(self, exportadores):
        formatos_disponibles = []
        clases_exportadores = {}
        for nombre, exp in inspect.getmembers(exportadores, inspect.isclass):
            if exp is exportador.ExportadorErs or\
                    not hasattr(exp, 'registrar_formato'):
                continue
            formato = exp.registrar_formato()
            formatos_disponibles.append(formato)
            clave_exportador = formato[1].partition('.')[-1]
            clases_exportadores[clave_exportador] = exp
        return formatos_disponibles, clases_exportadores

    def __configura_ventana_principal(self):
        self.__muestra_documento()
        barra_menu = tk.Menu(self.__ventana_principal)

        archivo = tk.Menu(barra_menu)
        archivo.add_command(label='Generar documento',
                            command=self.__finalizar_documento,
                            accelerator='F8')
        archivo.add_separator()
        archivo.add_command(label='Salir', command=self.__cerrar_ventana)
        barra_menu.add_cascade(label='Archivo', menu=archivo)

        editar = tk.Menu(barra_menu)
        editar.add_command(label='Nombre del proyecto',
                           command=self.__editar_nombre_proyecto,
                           accelerator='F4')
        editar.add_command(label='Responsables de emisión',
                           command=self.__editar_responsables_emision,
                           accelerator='F5')
        editar.add_command(label='Referencias',
                           command=self.__muestra_referencias, accelerator='F6')
        barra_menu.add_cascade(label='Editar', menu=editar)
        self.__construye_barra_menu_comun(self.__ventana_principal, barra_menu)

        self.__ventana_principal.bind('<F4>',
                                      lambda e: self.__editar_nombre_proyecto())
        self.__ventana_principal.bind(
            '<F5>', lambda e: self.__editar_responsables_emision())
        self.__ventana_principal.bind('<F6>',
                                      lambda e: self.__muestra_referencias())
        self.__ventana_principal.bind('<F8>',
                                      lambda e: self.__finalizar_documento())
        self.__ventana_principal.mainloop()

    def __muestra_documento(self):
        self.__ventana_principal.nombre_proyecto = ConfigIU.crea_etiqueta(
            self.__ventana_principal,
            self.__especificacion_requerimientos.titulo, es_titulo=True,
            row=0, column=0, columnspan=3)
        icono_ver = ConfigIU.carga_imagen(self.__ventana_principal, 'ver.png')
        icono_editar = ConfigIU.carga_imagen(self.__ventana_principal,
                                             'editar.png')
        # esto previene un bug con PhotoImage que recicla la imagen en uso
        self.__ventana_principal.icono_ver = icono_ver
        self.__ventana_principal.icono_editar = icono_editar

        ayuda_no_requerimientos = ContenidoAyuda.obtener_ayuda(
            ContenidoAyuda.AYUDA_NO_REQUERIMIENTOS)
        ConfigIU.crea_etiqueta(
            self.__ventana_principal, 'Número de Requerimientos:',
            ayuda_no_requerimientos, row=1, column=0, sticky='W')
        self.__etiqueta_num_reqs = ConfigIU.crea_etiqueta(
            self.__ventana_principal,
            len(self.__especificacion_requerimientos.requerimientos),
            ayuda_no_requerimientos, row=1, column=1)
        boton = tk.Button(self.__ventana_principal, image=icono_ver,
                          command=self.__muestra_requerimientos)
        boton.grid(row=1, column=2, sticky='E')
        ToolTip(boton, 'Ver Requerimientos (F2)')

        ayuda_len_glosario = ContenidoAyuda.obtener_ayuda(
            ContenidoAyuda.AYUDA_LEN_GLOSARIO)
        ConfigIU.crea_etiqueta(self.__ventana_principal, 'Tamaño del Glosario:',
                               ayuda_len_glosario, row=2, column=0, sticky='W')
        self.__etiqueta_tam_glosario = ConfigIU.crea_etiqueta(
            self.__ventana_principal,
            len(self.__especificacion_requerimientos.glosario),
            ayuda_len_glosario, row=2, column=1)
        boton = tk.Button(self.__ventana_principal, image=icono_ver,
                          command=self.__muestra_glosario)
        boton.grid(row=2, column=2, sticky='E')
        ToolTip(boton, 'Ver Glosario (F3)')

        ayuda_no_referencias = ContenidoAyuda.obtener_ayuda(
            ContenidoAyuda.AYUDA_NO_REFERENCIAS)
        ConfigIU.crea_etiqueta(
            self.__ventana_principal, 'Número de Referencias:',
            ayuda_no_referencias, row=3, column=0, sticky='W')
        self.__etiqueta_num_refs = ConfigIU.crea_etiqueta(
            self.__ventana_principal,
            len(self.__especificacion_requerimientos.referencias),
            ayuda_no_referencias, row=3, column=1)
        boton = tk.Button(self.__ventana_principal, image=icono_editar,
                          command=self.__muestra_referencias)
        boton.grid(row=3, column=2, sticky='E')
        ToolTip(boton, 'Editar Referencias (F6)')

        ayuda_responsables = ContenidoAyuda.obtener_ayuda(
            ContenidoAyuda.AYUDA_RESPONSABLES_EMISION)
        ConfigIU.crea_etiqueta(
            self.__ventana_principal, 'Responsables de emisión:',
            ayuda_responsables, row=4, column=0, sticky='W')
        self.__etiqueta_responsables_emision = ConfigIU.crea_etiqueta(
            self.__ventana_principal, ', '.join(
                self.__especificacion_requerimientos.responsables_emision
                    .values()), ayuda_responsables, row=4, column=1)
        boton = tk.Button(self.__ventana_principal, image=icono_editar,
                          command=self.__editar_responsables_emision)
        boton.grid(row=4, column=2, sticky='E')
        ToolTip(boton, 'Editar Responsables de Emisión (F5)')

        separador = ttk.Separator(self.__ventana_principal, orient='horizontal')
        separador.grid(row=5, column=0, columnspan=3, sticky='EW')

        ayuda_version = ContenidoAyuda.obtener_ayuda(
            ContenidoAyuda.AYUDA_VERSION)
        ConfigIU.crea_etiqueta(self.__ventana_principal, 'Versión:',
                               ayuda_version, row=6, column=0, sticky='W')
        self.__texto_version = ConfigIU.campo_texto_hint(
            self.__ventana_principal,
            self.__especificacion_requerimientos.version)
        self.__texto_version.grid(row=6, column=1, columnspan=2)

        ayuda_alcances = ContenidoAyuda.obtener_ayuda(
            ContenidoAyuda.AYUDA_ALCANCES)
        ConfigIU.crea_etiqueta(self.__ventana_principal, 'Alcances:',
                               ayuda_alcances, row=7, column=0, sticky='NW')
        self.__texto_alcances = tk.Text(self.__ventana_principal,
                                        height=10, wrap=tk.WORD)
        self.__texto_alcances.insert(
            tk.INSERT, self.__especificacion_requerimientos.alcances)
        self.__texto_alcances.grid(row=7, column=1, columnspan=2)

        separador = ttk.Separator(self.__ventana_principal, orient='horizontal')
        separador.grid(row=8, column=0, columnspan=3, sticky='EW')

        icono_estado = ConfigIU.carga_imagen(self.__ventana_principal,
                                             'info.png')
        self.__ventana_principal.icono_estado = icono_estado

        self.__ventana_principal.etiqueta_estado = ConfigIU.crea_etiqueta(
            self.__ventana_principal, icono_estado, row=9, column=0, sticky='W')
        self.__ventana_principal.barra_estado = ConfigIU.crea_etiqueta(
            self.__ventana_principal,
            ContenidoAyuda.obtener_descripcion_estado(self),
            row=9, column=0, columnspan=3, padx=30, sticky='W')

        self.__estado = Requex.ESTADO_PANTALLA_PRINCIPAL

    def __muestra_requerimientos(self):
        nombres_elementos, elementos_lista =\
            ConfigIU.formatea_requerimientos(
                self.__especificacion_requerimientos.requerimientos)
        titulo_lista = 'Lista de Requerimientos del proyecto "{}"'.format(
            self.nombre_proyecto)
        self.__muestra_ventana_inspeccion(
            Requex.ESTADO_INSPECCION_REQ, titulo_lista,
            'Número de requerimientos', nombres_elementos, elementos_lista,
            'requerimiento', self.__agregar_requerimiento,
            self.__suprimir_requerimiento, self.__editar_requerimiento)

    def __muestra_papelera_requerimientos(self):
        nombres_elementos, elementos_lista =\
            ConfigIU.formatea_requerimientos(
                self.__papelera_requerimientos)
        titulo_lista = 'Papelera de requerimientos'
        self.__muestra_ventana_inspeccion(
            Requex.ESTADO_RECICLANDO_REQ, titulo_lista,
            'Requerimientos reciclables', nombres_elementos, elementos_lista,
            'requerimiento', self.__recuperar_requerimiento,
            self.__descartar_requerimiento, etiqueta_alta='Recuperar',
            deshabilitar_alta=True, renglones_lista=20)

    def __muestra_glosario(self):
        nombres_elementos, elementos_lista = ConfigIU.formatea_glosario(
            self.__especificacion_requerimientos.glosario)
        titulo_lista = 'Glosario del proyecto "{}"'.format(
            self.nombre_proyecto)
        self.__muestra_ventana_inspeccion(
            Requex.ESTADO_INSPECCION_GLOSARIO, titulo_lista,
            'Tamaño del glosario', nombres_elementos, elementos_lista,
            'entrada', self.__agregar_entrada_glosario,
            self.__suprimir_entrada_glosario, self.__editar_entrada_glosario)

    def __editar_responsables_emision(self):
        nombres_elementos, elementos_lista =\
            ConfigIU.formatea_responsables_emision(
                self.__especificacion_requerimientos.responsables_emision)
        titulo_lista = 'Responsables de emisión'
        self.__muestra_ventana_inspeccion(
            Requex.ESTADO_INSPECCION_RESPONSABLES, titulo_lista,
            'Número de responsables', nombres_elementos, elementos_lista,
            'responsable', self.__agregar_responsable_emision,
            self.__suprimir_responsable_emision, renglones_lista=10)

    def __muestra_referencias(self):
        nombre_elementos, elementos_lista = ConfigIU.formatea_referencias(
            self.__especificacion_requerimientos.referencias)
        titulo_lista = 'Referencias del proyecto "{}"'.format(
            self.nombre_proyecto)
        self.__muestra_ventana_inspeccion(
            Requex.ESTADO_INSPECCION_REFERENCIAS, titulo_lista,
            'Número de referencias', nombre_elementos, elementos_lista,
            'referencia', self.__agregar_referencia, self.__suprimir_referencia,
            renglones_lista=20)

    def __muestra_ventana_inspeccion(self, estado, titulo_iu,
                                     texto_num_elementos, etiquetas_elementos,
                                     elementos_lista, nombre_elementos,
                                     comando_alta, comando_baja,
                                     comando_modificacion=None,
                                     etiqueta_alta='Agregar',
                                     deshabilitar_alta=False,
                                     renglones_lista=25):
        self.__estado = estado
        iu_lista = self.__prepara_ventana_analisis(titulo_iu)
        self.__construye_barra_menu_comun(iu_lista)
        lista_seleccionable = self.__construye_lista_inspeccion(
            iu_lista, texto_num_elementos, etiquetas_elementos,
            elementos_lista, renglones_lista)
        self.__agrega_botones_abm(
            iu_lista, nombre_elementos, etiqueta_alta, comando_alta,
            comando_baja, comando_modificacion, deshabilitar_alta)
        self.__construye_barra_estado_comun(iu_lista)
        if estado == Requex.ESTADO_INSPECCION_REQ:
            self.__iu_requerimientos = iu_lista
            self.__lista_requerimientos = lista_seleccionable
        elif estado == Requex.ESTADO_INSPECCION_GLOSARIO:
            self.__iu_glosario = iu_lista
            self.__entradas_glosario = lista_seleccionable
        elif estado == Requex.ESTADO_INSPECCION_RESPONSABLES:
            self.__iu_responsables_emision = iu_lista
            self.__lista_responsables_emision = lista_seleccionable
        elif estado == Requex.ESTADO_INSPECCION_REFERENCIAS:
            self.__iu_referencias = iu_lista
            self.__lista_referencias = lista_seleccionable
        elif estado == Requex.ESTADO_RECICLANDO_REQ:
            self.__iu_papelera_requerimientos = iu_lista
            self.__lista_requerimientos_reciclados = lista_seleccionable
        else:
            logger.error('Se ha construido una lista de inspección con un '
                         + 'estado inválido; es posible que se presenten '
                         + 'errores durante la ejecución')
        iu_lista.mainloop()

    def __editar_nombre_proyecto(self):
        ventana_nombre = self.__prepara_ventana_analisis('Nombre del proyecto')

        def comando_renombrar():
            try:
                nombre_proyecto = iu_proyecto.get()
                self.nombre_proyecto = nombre_proyecto
                self.__ventana_principal.nombre_proyecto.config(
                    text=self.__especificacion_requerimientos.titulo)
            except AttributeError as e:
                messagebox.showwarning('Volver', e)
            self.__cerrar_ventana()

        iu_proyecto = self.__prepara_formulario(
            ventana_nombre, 3, comando_renombrar, campos_texto={
                'Nombre del proyecto': self.nombre_proyecto
                })
        ventana_nombre.mainloop()

    def __agregar_requerimiento(self):
        iu_agregar_requerimiento = self.__prepara_ventana_analisis(
            'Agregar requerimiento')

        def comando_agregar():
            self.__siguiente_id_requerimiento += 1
            id = self.__siguiente_id_requerimiento
            clausula_especifica = iu_clausula_especifica.get()
            clausula = (iu_clausula_org.get(), clausula_especifica)\
                if clausula_especifica else iu_clausula_org.get()
            requerimiento = ConfigIU.verifica_atributos_requerimiento(
                id, iu_titulo.get(), iu_descripcion.get(),
                iu_estado_swebok.get(), iu_estado_essence.get(),
                iu_prioridad.get(), iu_volatilidad.get(),
                iu_tipo.get(), iu_riesgo.get(), clausula)
            if requerimiento:
                self.__especificacion_requerimientos.agregar_requerimiento(
                    requerimiento)
                indice = len(self.__lista_requerimientos.get_children())
                nuevo_elemento = ConfigIU.formatea_requerimientos(
                    {requerimiento.id: requerimiento})[1][0]
                self.__relacion_indice_lista_id =\
                    ConfigIU.actualiza_lista_seleccionable(
                        self.__lista_requerimientos, indice,
                        self.__relacion_indice_lista_id, nuevo_elemento)
                cuenta_requerimientos = str(len(
                    self.__especificacion_requerimientos.requerimientos))
                self.__actualiza_estado_inspeccion(
                    self.__iu_requerimientos, 'check.png',
                    'Requerimiento {} agregado exitosamente'
                    .format(requerimiento.id), self.__etiqueta_num_reqs,
                    'Número de requerimientos: ', cuenta_requerimientos)
                self.__cerrar_ventana()

        iu_titulo, iu_descripcion, iu_riesgo, iu_tipo, iu_prioridad,\
            iu_volatilidad, iu_estado_swebok, iu_estado_essence,\
            iu_clausula_org, iu_clausula_especifica =\
            self.__mostrar_requerimiento(iu_agregar_requerimiento,
                                         comando_agregar)
        iu_agregar_requerimiento.mainloop()

    def __suprimir_requerimiento(self):
        id = self.__relacion_indice_lista_id[self.__eleccion_iu]
        requerimiento = self.__especificacion_requerimientos.requerimientos[id]
        self.__papelera_requerimientos[id] = requerimiento
        self.__especificacion_requerimientos.suprimir_requerimiento(id)
        self.__relacion_indice_lista_id =\
            ConfigIU.actualiza_lista_seleccionable(
                self.__lista_requerimientos, self.__eleccion_iu,
                self.__relacion_indice_lista_id)
        cuenta_requerimientos = str(len(
            self.__especificacion_requerimientos.requerimientos))
        self.__actualiza_estado_inspeccion(
            self.__iu_requerimientos, 'check.png',
            'Requerimiento {} movido a la papelera exitosamente'.format(id),
            self.__etiqueta_num_reqs, 'Número de requerimientos: ',
            cuenta_requerimientos)
        self.__deshabilita_botones_cambios()

    def __editar_requerimiento(self):
        id = self.__relacion_indice_lista_id[self.__eleccion_iu]
        requerimiento = self.__especificacion_requerimientos.requerimientos[id]
        iu_editar_requerimiento = self.__prepara_ventana_analisis(
            'Editar requerimiento')

        def comando_editar():
            clausula_especifica = iu_clausula_especifica.get()
            clausula = (iu_clausula_org.get(), clausula_especifica)\
                if clausula_especifica else iu_clausula_org.get()
            requerimiento = ConfigIU.verifica_atributos_requerimiento(
                id, iu_titulo.get(), iu_descripcion.get(),
                iu_estado_swebok.get(), iu_estado_essence.get(),
                iu_prioridad.get(), iu_volatilidad.get(),
                iu_tipo.get(), iu_riesgo.get(), clausula)
            if requerimiento:
                self.__especificacion_requerimientos.suprimir_requerimiento(id)
                self.__especificacion_requerimientos.agregar_requerimiento(
                    requerimiento)
                requerimiento = ConfigIU.formatea_requerimientos(
                    {requerimiento.id: requerimiento})[1][0]
                self.__relacion_indice_lista_id =\
                    ConfigIU.actualiza_lista_seleccionable(
                        self.__lista_requerimientos, self.__eleccion_iu,
                        self.__relacion_indice_lista_id, requerimiento)
                self.__actualiza_estado_inspeccion(
                    self.__iu_requerimientos, 'check.png',
                    'Requerimiento {} actualizado exitosamente'.format(id))
                self.__cerrar_ventana()

        iu_titulo, iu_descripcion, iu_riesgo, iu_tipo, iu_prioridad,\
            iu_volatilidad, iu_estado_swebok, iu_estado_essence,\
            iu_clausula_org, iu_clausula_especifica =\
            self.__mostrar_requerimiento(iu_editar_requerimiento,
                                         comando_editar, requerimiento)
        iu_editar_requerimiento.mainloop()

    def __descartar_requerimiento(self):
        id = self.__relacion_indice_lista_id[self.__eleccion_iu]
        pregunta = 'Los requerimientos eliminados de la papelera no pueden ser'\
            + ' recuperados'
        respuesta = messagebox.askyesno('¿Eliminar definitivamente?', pregunta)
        if not respuesta:
            self.__actualiza_estado_inspeccion(
                self.__iu_papelera_requerimientos, 'check.png',
                'El requerimiento {} permanece en la papelera'.format(id))
            return
        del self.__papelera_requerimientos[id]
        self.__relacion_indice_lista_id =\
            ConfigIU.actualiza_lista_seleccionable(
                self.__lista_requerimientos_reciclados, self.__eleccion_iu,
                self.__relacion_indice_lista_id)
        self.__actualiza_estado_inspeccion(
            self.__iu_papelera_requerimientos, 'warning.png',
            'El requerimiento {} ha sido eliminado definitivamente'.format(id),
            mensaje_cuenta='Requerimientos reciclables: ',
            cuenta_local=len(self.__papelera_requerimientos))
        self.__deshabilita_botones_cambios(True)

    def __recuperar_requerimiento(self):
        id = self.__relacion_indice_lista_id[self.__eleccion_iu]
        requerimiento = self.__papelera_requerimientos[id]
        del self.__papelera_requerimientos[id]
        self.__especificacion_requerimientos.agregar_requerimiento(
            requerimiento)
        self.__relacion_indice_lista_id =\
            ConfigIU.actualiza_lista_seleccionable(
                self.__lista_requerimientos_reciclados, self.__eleccion_iu,
                self.__relacion_indice_lista_id)
        cuenta_requerimientos = str(len(
            self.__especificacion_requerimientos.requerimientos))
        self.__actualiza_estado_inspeccion(
            self.__iu_papelera_requerimientos, 'check.png',
            'El requerimiento {} ha sido restaurado'.format(id),
            self.__etiqueta_num_reqs, 'Requerimientos reciclables: ',
            cuenta_requerimientos, len(self.__papelera_requerimientos))
        self.__deshabilita_botones_cambios(True)

    def __agregar_entrada_glosario(self):
        iu_agregar_entrada_glosario = self.__prepara_ventana_analisis(
            'Agregar entrada al glosario')

        def comando_agregar():
            terminos = iu_terminos.get().split(',')
            terminos = [t.strip() for t in terminos]
            definicion = iu_definicion.get()
            try:
                id = self.__especificacion_requerimientos.glosario\
                    .agregar_entrada(terminos[0], definicion)
                for term in terminos[1:]:
                    self.__especificacion_requerimientos.glosario\
                        .agregar_sinonimo(id, term)
                terminos_str = ', '.join(terminos)
                glosario = {id: self.__especificacion_requerimientos.glosario
                            .obtener_entrada(id)}
                entrada = ConfigIU.formatea_glosario(glosario)[1][0]
                indice = len(self.__entradas_glosario.get_children())
                self.__relacion_indice_lista_id =\
                    ConfigIU.actualiza_lista_seleccionable(
                        self.__entradas_glosario, indice,
                        self.__relacion_indice_lista_id, entrada)
                len_glosario = str(len(
                    self.__especificacion_requerimientos.glosario))
                self.__actualiza_estado_inspeccion(
                    self.__iu_glosario, 'check.png',
                    '"{}" agregado al glosario'.format(terminos_str),
                    self.__etiqueta_tam_glosario, 'Tamaño del glosario: ',
                    len_glosario)
                self.__cerrar_ventana()
            except AttributeError as e:
                messagebox.showwarning('Volver', e)

        iu_terminos, iu_definicion = self.__mostrar_entrada_glosario(
            iu_agregar_entrada_glosario, comando_agregar)
        iu_agregar_entrada_glosario.mainloop()

    def __suprimir_entrada_glosario(self):
        self.__especificacion_requerimientos.glosario.suprimir_entrada(
            self.__relacion_indice_lista_id[self.__eleccion_iu])
        self.__relacion_indice_lista_id =\
            ConfigIU.actualiza_lista_seleccionable(
                self.__entradas_glosario, self.__eleccion_iu,
                self.__relacion_indice_lista_id)
        len_glosario = str(len(self.__especificacion_requerimientos.glosario))
        self.__actualiza_estado_inspeccion(
            self.__iu_glosario, 'check.png', 'Término eliminado exitosamente',
            self.__etiqueta_tam_glosario, 'Tamaño del glosario: ', len_glosario)
        self.__deshabilita_botones_cambios()

    def __editar_entrada_glosario(self):
        id = self.__relacion_indice_lista_id[self.__eleccion_iu]
        entrada = self.__especificacion_requerimientos.glosario.obtener_entrada(
            id)
        iu_editar_entrada_glosario = self.__prepara_ventana_analisis(
            'Editar entrada del glosario')

        def comando_editar():
            terminos = iu_terminos.get().split(',')
            terminos = [t.strip() for t in terminos]
            definicion = iu_definicion.get()
            try:
                self.__especificacion_requerimientos.glosario.editar_entrada(
                    id, terminos[0], definicion)
                for term in terminos[1:]:
                    self.__especificacion_requerimientos.glosario\
                        .agregar_sinonimo(id, term)
                entrada = self.__especificacion_requerimientos.glosario\
                    .obtener_entrada(id)
                entrada = ConfigIU.formatea_glosario({id: entrada})[1][0]
                self.__relacion_indice_lista_id =\
                    ConfigIU.actualiza_lista_seleccionable(
                        self.__entradas_glosario, self.__eleccion_iu,
                        self.__relacion_indice_lista_id, entrada)
                self.__actualiza_estado_inspeccion(
                    self.__iu_glosario, 'check.png',
                    'Término modificado exitosamente')
                self.__cerrar_ventana()
            except AttributeError as e:
                messagebox.showwarning('Volver', e)

        iu_terminos, iu_definicion = self.__mostrar_entrada_glosario(
            iu_editar_entrada_glosario, comando_editar, entrada.terminos,
            entrada.definicion)
        iu_editar_entrada_glosario.mainloop()

    def __agregar_referencia(self):
        iu_agregar_referencia = self.__prepara_ventana_analisis(
            'Agregar referencia')

        def comando_agregar():
            try:
                referencia = iu_referencia.get()
                id = self.__especificacion_requerimientos.agregar_referencia(
                    referencia)
                indice = len(self.__lista_referencias.get_children())
                referencia = ConfigIU.formatea_referencias(
                    {id: referencia})[1][0]
                self.__relacion_indice_lista_id =\
                    ConfigIU.actualiza_lista_seleccionable(
                        self.__lista_referencias, indice,
                        self.__relacion_indice_lista_id, referencia)
                cuenta_referencias = str(len(
                    self.__especificacion_requerimientos.referencias))
                self.__actualiza_estado_inspeccion(
                    self.__iu_referencias, 'check.png',
                    'Referencia "{}" agregada exitosamente'.format(id),
                    self.__etiqueta_num_refs, 'Número de referencias: ',
                    cuenta_referencias)
                self.__cerrar_ventana()
            except AttributeError as e:
                messagebox.showwarning('Volver', e)

        iu_referencia = self.__mostrar_referencia(iu_agregar_referencia,
                                                  comando_agregar)
        iu_agregar_referencia.mainloop()

    def __suprimir_referencia(self):
        id_original = self.__relacion_indice_lista_id[self.__eleccion_iu]
        self.__especificacion_requerimientos.suprimir_referencia(id_original)
        self.__relacion_indice_lista_id =\
            ConfigIU.actualiza_lista_seleccionable(
                self.__lista_referencias, self.__eleccion_iu,
                self.__relacion_indice_lista_id)
        cuenta_referencias = str(len(
            self.__especificacion_requerimientos.referencias))
        self.__actualiza_estado_inspeccion(
            self.__iu_referencias, 'check.png',
            'Referencia "{}" eliminada exitosamente'.format(id_original),
            self.__etiqueta_num_refs, 'Número de referencias: ',
            cuenta_referencias)
        self.__deshabilita_botones_cambios()

    def __agregar_responsable_emision(self):
        iu_agregar_responsable = self.__prepara_ventana_analisis(
            'Agregar responsable de emisión')

        def comando_agregar():
            responsable = iu_responsable.get()
            try:
                id = self.__especificacion_requerimientos\
                    .agregar_responsable_emision(responsable)
                indice = len(self.__lista_responsables_emision.get_children())
                responsable = ConfigIU.formatea_responsables_emision(
                    {id: responsable})[1][0]
                self.__relacion_indice_lista_id =\
                    ConfigIU.actualiza_lista_seleccionable(
                        self.__lista_responsables_emision, indice,
                        self.__relacion_indice_lista_id, responsable)
                self.__etiqueta_responsables_emision.config(
                    text=', '.join(self.__especificacion_requerimientos
                                   .responsables_emision.values()))
                self.__actualiza_estado_inspeccion(
                    self.__iu_responsables_emision, 'check.png',
                    'Responsable "{}" agregado exitosamente'.format(id),
                    mensaje_cuenta='Número de responsables: ',
                    cuenta_principal=len(self.__especificacion_requerimientos
                                         .responsables_emision))
                self.__cerrar_ventana()
            except AttributeError as e:
                messagebox.showwarning('Volver', e)

        iu_responsable = self.__mostrar_responsable_emision(
            iu_agregar_responsable, comando_agregar)
        iu_agregar_responsable.mainloop()

    def __suprimir_responsable_emision(self):
        id = self.__relacion_indice_lista_id[self.__eleccion_iu]
        self.__especificacion_requerimientos.suprimir_responsable_emision(id)
        self.__relacion_indice_lista_id =\
            ConfigIU.actualiza_lista_seleccionable(
                self.__lista_responsables_emision, self.__eleccion_iu,
                self.__relacion_indice_lista_id)
        self.__etiqueta_responsables_emision.config(
            text=', '.join(self.__especificacion_requerimientos
                           .responsables_emision.values()))
        self.__actualiza_estado_inspeccion(
            self.__iu_responsables_emision, 'check.png',
            'Responsable "{}" eliminado exitosamente'.format(id),
            mensaje_cuenta='Número de responsables: ',
            cuenta_principal=len(
                self.__especificacion_requerimientos.responsables_emision))
        self.__deshabilita_botones_cambios()

    def __finalizar_documento(self):
        if self.verificar_completo() == 2:
            pregunta = 'Despúes de terminar ya no podrá hacer cambios con '\
                  + ' esta herramienta a la ERS; a menos que repita el'\
                  + ' proceso completo de extracción y análisis'
        else:
            pregunta = 'El documento aún contiene valores vacíos o por defecto'
        respuesta = messagebox.askyesno('¿Terminar?', pregunta)
        if respuesta:
            self.__estado = Requex.ESTADO_EXPORTANDO_ERS
            self.__especificacion_requerimientos.version =\
                self.__texto_version.get()
            self.__especificacion_requerimientos.alcances =\
                self.__texto_alcances.get('1.0', tk.END)
            self.__especificacion_requerimientos.finalizar()
            exportadores, clases_exp = self.__registrar_exportadores(exportador)
            archivo_destino = filedialog.asksaveasfilename(
                title='Guardar ERS como...', filetypes=exportadores)
            exp = clases_exp[archivo_destino.partition('.')[-1]]
            formato = exp(self.__especificacion_requerimientos)
            self.__exportar_documento(archivo_destino, formato)

    def __exportar_documento(self, archivo_destino, formato):
        formato.exportar(archivo_destino)
        self.__cerrar_aplicacion()

    def __eleccion_lista_iu(self, evento):
        lista = evento.widget
        self.__eleccion_iu = int(lista.focus())
        if not self.__botones_iu:
            return
        for boton in self.__botones_iu.values():
            boton['state'] = 'normal'

    def __agrega_botones_abm(self, ventana, tipo_entidad, etiqueta_alta,
                             alta=None, baja=None, modificacion=None,
                             deshabilitar_alta=False, row=0):
        self.__botones_iu = {}
        if alta:
            estado = 'disabled' if deshabilitar_alta else 'normal'
            botonA = ConfigIU.construye_boton(
                ventana, '{} {}'.format(etiqueta_alta, tipo_entidad), alta,
                row, estado=estado, padx=50, sticky='E')
            self.__botones_iu['alta'] = botonA
        if baja:
            botonB = ConfigIU.construye_boton(ventana, 'Eliminar', baja,
                                              row, column=1, estado='disabled',
                                              padx=50, sticky='E')
            self.__botones_iu['baja'] = botonB
        if modificacion:
            botonM = ConfigIU.construye_boton(ventana, 'Editar', modificacion,
                                              row, column=1, estado='disabled',
                                              sticky='W')
            self.__botones_iu['modificacion'] = botonM

    def __agrega_botones_sn(self, ventana, aceptar, row,
                            titulo_aceptar='Aceptar',
                            titulo_cancelar='Cancelar', column=None):
        botonAceptar = ConfigIU.construye_boton(
            ventana, titulo_aceptar, aceptar, row,
            column=column if column is not None else 1, sticky='W')

        def cancelar():
            self.__accion_cancelada = True
            self.__cerrar_ventana()
        ConfigIU.construye_boton(
            ventana, titulo_cancelar, cancelar, row,
            column=column if column is not None else 1, sticky='E')
        return botonAceptar

    def __construye_barra_estado_comun(self, ventana, row=2):
        icono_estado = ConfigIU.carga_imagen(self.__ventana_principal,
                                             'info.png')
        ventana.icono_estado = icono_estado
        ventana.etiqueta_estado = ConfigIU.crea_etiqueta(
            ventana, icono_estado, row=row, column=0, sticky='W')
        mensaje_estado = ContenidoAyuda.obtener_descripcion_estado(self)
        ventana.barra_estado = ConfigIU.crea_etiqueta(
            ventana, mensaje_estado, row=row, column=0, columnspan=3,
            padx=30, sticky='W')

    def __cerrar_ventana(self):
        ventana = self.__iu_activa
        if not self.__libera_ventanas(ventana):
            return
        self.__reportar_accion_cancelada()
        raiz = ventana.nametowidget(ventana.winfo_parent())
        if raiz:
            raiz.deiconify()
            self.__iu_activa = raiz
            if raiz is self.__ventana_principal:
                self.__estado = Requex.ESTADO_PANTALLA_PRINCIPAL
                mensaje_ayuda, archivo_icono =\
                    ContenidoAyuda.obtener_descripcion_estado(self)
                icono_estado = ConfigIU.carga_imagen(self.__ventana_principal,
                                                     archivo_icono)
                self.__ventana_principal.icono_estado = icono_estado
                self.__ventana_principal.etiqueta_estado.config(
                    image=icono_estado)
                self.__ventana_principal.barra_estado.config(text=mensaje_ayuda)
        if ventana is self.__ventana_principal:
            self.__cerrar_aplicacion()
        ventana.destroy()

    def __libera_ventanas(self, ventana_a_cerrar):
        if ventana_a_cerrar is self.__ventana_principal:
            return ConfigIU.confirma_cerrar_aplicacion()
        elif ventana_a_cerrar is self.__iu_requerimientos:
            self.__iu_requerimientos = None
        elif ventana_a_cerrar is self.__iu_glosario:
            self.__iu_glosario = None
        elif ventana_a_cerrar is self.__iu_responsables_emision:
            self.__iu_responsables_emision = None
        elif ventana_a_cerrar is self.__iu_referencias:
            self.__iu_referencias = None
        elif ventana_a_cerrar is self.__iu_papelera_requerimientos:
            self.__iu_papelera_requerimientos = None
        return True

    def __reportar_accion_cancelada(self):
        if self.__accion_cancelada:
            ventana_inspeccion = None
            if self.__estado == Requex.ESTADO_INSPECCION_REQ:
                ventana_inspeccion = self.__iu_requerimientos
            elif self.__estado == Requex.ESTADO_INSPECCION_GLOSARIO:
                ventana_inspeccion = self.__iu_glosario
            elif self.__estado == Requex.ESTADO_INSPECCION_RESPONSABLES:
                ventana_inspeccion = self.__iu_responsables_emision
            elif self.__estado == Requex.ESTADO_INSPECCION_REFERENCIAS:
                ventana_inspeccion = self.__iu_referencias
            if ventana_inspeccion:
                self.__actualiza_estado_inspeccion(
                    ventana_inspeccion, 'warning.png', 'Acción cancelada')
            self.__accion_cancelada = False

    def __actualiza_estado_inspeccion(
            self, ventana, imagen_estado, mensaje_estado,
            etiqueta_cuenta_principal=None, mensaje_cuenta=None,
            cuenta_principal=None, cuenta_local=None):
        if etiqueta_cuenta_principal and cuenta_principal is not None:
            etiqueta_cuenta_principal.config(text=cuenta_principal)
        if mensaje_cuenta and cuenta_principal is not None:
            ventana.etiqueta_cuenta.config(
                text=mensaje_cuenta + str(cuenta_principal))
        if mensaje_cuenta and cuenta_local is not None:
            ventana.etiqueta_cuenta.config(
                text=mensaje_cuenta + str(cuenta_local))
        icono_estado = ConfigIU.carga_imagen(ventana, imagen_estado)
        ventana.icono_estado = icono_estado
        ventana.etiqueta_estado.config(image=icono_estado)
        ventana.barra_estado.config(text=mensaje_estado)

    def __prepara_ventana_analisis(self, titulo, ancho=None, alto=None):
        raiz = self.__iu_activa
        ventana = ConfigIU.crea_ventana(titulo, raiz)
        ventana.protocol("WM_DELETE_WINDOW", self.__cerrar_ventana)

        self.__iu_activa = ventana
        if ancho and alto:
            ventana.geometry(str(ancho) + 'x' + str(alto))
            ventana.resizable(0, 0)
            marco = tk.Frame(ventana)
            marco.pack_propagate(0)
            marco.pack(fill=tk.BOTH, expand=1)
            ventana = marco
        if raiz:
            raiz.withdraw()
        return ventana

    def __mostrar_requerimiento(self, ventana, comando_guardar,
                                requerimiento=None):
        if requerimiento:
            ConfigIU.crea_etiqueta(
                ventana, 'Requerimiento {}'.format(requerimiento.id),
                es_titulo=True, row=2, column=0, columnspan=3)
        estados_swebok, estados_essence =\
            Requerimiento.obtener_estados_validos()
        estados_essence.append('Ninguno')
        tipos, clausulas_orgs = \
            Requerimiento.obtener_clausulas_organizacion_validas()
        clausulas_orgs = ['<Usar el tipo del requerimiento>'] + clausulas_orgs
        clausula_actual = requerimiento.clausula_organizacion\
            if requerimiento else None
        if not clausula_actual:
            clausula_actual = (None, None)
        if clausula_actual[0] in tipos:
            clausula_actual = (clausulas_orgs[0], clausula_actual[1])
        iu_titulo, iu_descripcion, iu_riesgo, iu_tipo, iu_prioridad,\
            iu_volatilidad, iu_estado_swebok, iu_estado_essence,\
            iu_clausula_org = self.__prepara_formulario(
                ventana, 3, campos_texto={
                    'Título:': requerimiento.titulo if requerimiento else None,
                    'Descripción:':
                        requerimiento.descripcion if requerimiento else None,
                    'Riesgo:': requerimiento.riesgo if requerimiento else None
                     }, ayuda_textos={
                     'Título:': ContenidoAyuda.obtener_ayuda(
                        ContenidoAyuda.AYUDA_TITULO_REQ),
                     'Descripción:': ContenidoAyuda.obtener_ayuda(
                        ContenidoAyuda.AYUDA_DESCRIPCION_REQ),
                     'Riesgo:': ContenidoAyuda.obtener_ayuda(
                        ContenidoAyuda.AYUDA_RIESGO_REQ)
                     }, selectores={
                     'Tipo:': (tipos, requerimiento.tipo
                               if requerimiento else None),
                     'Prioridad:': (range(11), requerimiento.prioridad
                                    if requerimiento else None),
                     'Volatilidad:': (range(11), requerimiento.volatilidad
                                      if requerimiento else None),
                     'Estado del requerimiento':
                     (estados_swebok, requerimiento.estado[0] if requerimiento
                      else None),
                     '': (estados_essence, requerimiento.estado[1]
                          if requerimiento else 'Ninguno'),
                     'Cláusula de organización':
                     (clausulas_orgs, clausula_actual[0])
                     }, ayuda_selectores={
                     'Tipo:': ContenidoAyuda.obtener_ayuda(
                        ContenidoAyuda.AYUDA_TIPO_REQ),
                     'Prioridad:': ContenidoAyuda.obtener_ayuda(
                        ContenidoAyuda.AYUDA_PRIORIDAD_REQ),
                     'Volatilidad:': ContenidoAyuda.obtener_ayuda(
                        ContenidoAyuda.AYUDA_VOLATILIDAD_REQ),
                     'Estado del requerimiento': ContenidoAyuda.obtener_ayuda(
                        ContenidoAyuda.AYUDA_ESTADO_SWEBOK_REQ),
                     '': ContenidoAyuda.obtener_ayuda(
                        ContenidoAyuda.AYUDA_ESTADO_ESSENCE_REQ),
                     'Cláusula de organización': ContenidoAyuda.obtener_ayuda(
                        ContenidoAyuda.AYUDA_CLAUSULA_ORG_REQ)
                 })
        iu_clausula_especifica = self.__prepara_formulario(
            ventana, 15, comando_guardar, campos_texto={
                'Descripción': clausula_actual[1]}, ayuda_textos={
                'Descripción': ContenidoAyuda.obtener_ayuda(
                    ContenidoAyuda.AYUDA_DESC_CLAUSULA_ORG_REQ)
                })
        return iu_titulo, iu_descripcion, iu_riesgo, iu_tipo[1],\
            iu_prioridad[1], iu_volatilidad[1], iu_estado_swebok[1],\
            iu_estado_essence[1], iu_clausula_org[1], iu_clausula_especifica

    def __mostrar_entrada_glosario(self, ventana, comando_guardar,
                                   terminos=None, definicion=None):
        iu_terminos, iu_definicion = self.__prepara_formulario(
            ventana, 2, comando_guardar, campos_texto={
                'Términos (separados por coma)':
                    ','.join(terminos) if terminos else None,
                'Definición': definicion
                }, ayuda_textos={
                'Términos (separados por coma)': ContenidoAyuda.obtener_ayuda(
                    ContenidoAyuda.AYUDA_TERMINOS_GLOSARIO),
                'Definición': ContenidoAyuda.obtener_ayuda(
                    ContenidoAyuda.AYUDA_DEFINICION_GLOSARIO)
                })
        return iu_terminos, iu_definicion

    def __mostrar_referencia(self, ventana, comando_guardar, referencia=None):
        iu_referencia = self.__prepara_formulario(
            ventana, 2, comando_guardar, campos_texto={
                'Referencia': referencia}, ayuda_textos={
                'Referencia': ContenidoAyuda.obtener_ayuda(
                    ContenidoAyuda.AYUDA_REFERENCIA)
                })
        return iu_referencia

    def __mostrar_responsable_emision(self, ventana, comando_guardar,
                                      responsable=None):
        iu_responsable = self.__prepara_formulario(
            ventana, 2, comando_guardar, campos_texto={
                'Responsable:': responsable}, ayuda_textos={
                'Responsable:': ContenidoAyuda.obtener_ayuda(
                    ContenidoAyuda.AYUDA_RESPONSABLES_EMISION)
                })
        return iu_responsable

    def __prepara_formulario(self, ventana, row, comando_aceptar=None,
                             campos_texto=None, ayuda_textos={},
                             selectores=None, ayuda_selectores={}):
        campos_iu = []
        if campos_texto:
            iu_campos_texto, rows_campos_texto =\
                ConfigIU.construye_campos_texto(ventana, campos_texto,
                                                ayuda_textos, row)
            campos_iu += iu_campos_texto
        else:
            rows_campos_texto = 0
        if selectores:
            iu_selectores, rows_selectores = ConfigIU.construye_selectores(
                ventana, selectores, ayuda_selectores, row + rows_campos_texto)
            campos_iu += iu_selectores
        else:
            rows_selectores = rows_campos_texto
        if comando_aceptar:
            self.__agrega_botones_sn(ventana, comando_aceptar, rows_selectores)
        return campos_iu[0] if len(campos_iu) == 1 else tuple(campos_iu)

    def __construye_lista_inspeccion(self, ventana, etiqueta_num_elementos,
                                     etiquetas_valores, valores, renglones):
        if not hasattr(ventana, 'icono_volver'):
            icono_volver = ConfigIU.carga_imagen(ventana, 'volver.png')
            ventana.icono_volver = icono_volver
        boton = tk.Button(ventana, image=ventana.icono_volver,
                          command=self.__cerrar_ventana)
        boton.grid(row=0, column=0, sticky='W')
        etiqueta_cuenta = ConfigIU.crea_etiqueta(
            ventana, '{}: {}'.format(etiqueta_num_elementos, len(valores)),
            row=0, column=2)
        ventana.etiqueta_cuenta = etiqueta_cuenta

        lista, self.__relacion_indice_lista_id =\
            ConfigIU.crea_lista_seleccionable(
                ventana, valores, etiquetas_valores, self.__eleccion_lista_iu,
                renglones)
        return lista

    def __deshabilita_botones_cambios(self, deshabilitar_alta=False):
        if deshabilitar_alta and 'alta' in self.__botones_iu:
            self.__botones_iu['alta']['state'] = 'disabled'
        if 'baja' in self.__botones_iu:
            self.__botones_iu['baja']['state'] = 'disabled'
        if 'modificacion' in self.__botones_iu:
            self.__botones_iu['modificacion']['state'] = 'disabled'

    def __construye_barra_menu_comun(self, ventana, barra_menu=None):
        if not barra_menu:
            barra_menu = tk.Menu(ventana)
        ver = tk.Menu(barra_menu)

        def cambia_ventana(muestra_ventana):
            if self.__iu_activa is not self.__ventana_principal:
                self.__cerrar_ventana()
            muestra_ventana()

        if self.__estado != Requex.ESTADO_INSPECCION_REQ:
            ver.add_command(label='Requerimientos',
                            command=self.__muestra_requerimientos,
                            accelerator='F2')
            ventana.bind('<F2>', lambda e: cambia_ventana(
                         self.__muestra_requerimientos))
        if self.__estado != Requex.ESTADO_INSPECCION_GLOSARIO:
            ver.add_command(label='Glosario', command=self.__muestra_glosario,
                            accelerator='F3')
            ventana.bind('<F3>',
                         lambda e: cambia_ventana(self.__muestra_glosario))
        if self.__estado != Requex.ESTADO_RECICLANDO_REQ:
            ver.add_command(label='Papelera de requerimientos',
                            command=self.__muestra_papelera_requerimientos,
                            accelerator='F7')
            ventana.bind('<F7>', lambda e: cambia_ventana(
                self.__muestra_papelera_requerimientos))
        barra_menu.add_cascade(label='Ver', menu=ver)

        ayuda = tk.Menu(barra_menu)
        ayuda.add_command(label='Ayuda para el análisis de requerimientos',
                          command=self.__muestra_ayuda, accelerator='F1')
        ayuda.add_command(label='Acerca de Requex',
                          command=self.__muestra_acercade)
        barra_menu.add_cascade(label='Ayuda', menu=ayuda)
        ventana.config(menu=barra_menu)
        ventana.bind('<F1>', lambda e: self.__muestra_ayuda())

    def __muestra_ayuda(self):
        iu_ayuda = self.__prepara_ventana_analisis('Ayuda de REQUEX')
        titulo_ayuda, contenido = ContenidoAyuda.obtener_ayuda_contextual(
            self.estado)
        ConfigIU.crea_etiqueta(iu_ayuda, titulo_ayuda, es_titulo=True, row=0)
        separador = ttk.Separator(iu_ayuda, orient='horizontal')
        separador.grid(row=1, sticky='EW')
        ConfigIU.crea_etiqueta(iu_ayuda, contenido, row=2)
        ConfigIU.construye_boton(iu_ayuda, 'Volver', self.__cerrar_ventana,
                                 row=3)
        iu_ayuda.mainloop()

    def __muestra_acercade(self):
        iu_acercade = self.__prepara_ventana_analisis('Acerca de REQUEX')
        logo = ConfigIU.carga_imagen(iu_acercade, 'icono_requex-256.png')
        iu_acercade.logo = logo
        ConfigIU.crea_etiqueta(iu_acercade, logo, row=0, column=0)
        descripcion, acercade = ContenidoAyuda.obtener_descripcion_acercade()
        ConfigIU.crea_etiqueta(iu_acercade, descripcion, row=1, column=0)
        ConfigIU.crea_etiqueta(iu_acercade, acercade, row=2, column=0)

        def mostrar_licencia():
            iu_acercade.withdraw()
            ConfigIU.muestra_url_repo_licencia(iu_acercade,
                                               self.__cerrar_ventana)
        self.__agrega_botones_sn(
            iu_acercade, mostrar_licencia, 3, 'Ver URL del proyecto y'
            + ' licencia', 'Volver', 0)
        iu_acercade.mainloop()

    def __cerrar_aplicacion(self):
        self.__ventana_principal.destroy()
        self.__pantalla_inicial.quit()
        del self.__hilo_carga
        quit()

    def formatea_texto(nombre_proyecto):
        nombre_proyecto = nombre_proyecto.strip()
        if len(nombre_proyecto.split()) > 1:
            nombre_proyecto = nombre_proyecto.capitalize()
        return nombre_proyecto


def iniciar_extraccion(dir_corpus, nombre_proyecto, pantalla_inicial):
    if not os.path.exists(dir_corpus):
        messagebox.showwarning(
            'Directorio a analizar inválido',
            'No es posible acceder al directorio "' + dir_corpus
            + '". Compruebe que la ruta es válida y que posee los '
            + 'permisos necesarios para acceder a su contenido')
        return

    proyecto = nombre_proyecto if type(nombre_proyecto) is str else\
        nombre_proyecto.get()
    proyecto = proyecto.replace('\n', '').replace('\r', '')
    proyecto = proyecto.strip()
    thread = HiloCarga(proyecto, dir_corpus)
    thread.start()
    pantalla_inicial.withdraw()
    try:
        rx = Requex(proyecto, thread, pantalla_inicial)
        rx.extrae_analiza(dir_corpus if type(dir_corpus) is str else
                          dir_corpus.get())
    except AttributeError as e:
        messagebox.showwarning('Problema de configuración', e)


def mostrar_ayuda():
    print(ContenidoAyuda.obtener_nombre_descripcion() + '\n'
          + ContenidoAyuda.obtener_descripcion_general()
          + '\nUso:\n'
          + '-h, --??\tMuestra este mensaje de ayuda y termina.\n'
          + '-n, --nombre\tNombre del proyecto a analizar. Puede modificar '
          + 'este valor durante el análisis de requerimientos.\n'
          + '-c, --corpus\tRuta al directorio con los archivos de texto plano '
          + 'correspondientes a las Tesis y Reportes de Servicio Social que '
          + 'describen el proyecto de software a analizar.\n\nEjemplos:\n'
          + '\t requex\n'
          + '\t requex -n "Proyecto ejemplo"\n'
          + '\t requex --nombre "Proyecto ejemplo" -c '
          + '"C:\\Users\\Administrator\\My documents\\directorio corpus"')
    quit()


def obtener_argumentos(args):
    try:
        args_opcionales, args_requeridos = getopt.getopt(
            args, 'hn:c:', ['??', 'nombre=', 'corpus='])
    except getopt.GetoptError:
        mostrar_ayuda()
    dir_corpus = None
    nombre_proyecto = None
    for arg, val in args_opcionales:
        if arg in ('-h', '--??'):
            mostrar_ayuda()
        elif arg in ('-n', '--nombre'):
            nombre_proyecto = val
        elif arg in ('-c', '--corpus'):
            dir_corpus = val
    return dir_corpus, nombre_proyecto


if __name__ == '__main__':
    dir_corpus, nombre_proyecto = obtener_argumentos(sys.argv[1:])
    pantalla_inicial = ConfigIU.construye_ventana_inicial(dir_corpus,
                                                          nombre_proyecto)
    if nombre_proyecto and dir_corpus:
        iniciar_extraccion(dir_corpus, nombre_proyecto, pantalla_inicial)
    else:
        pantalla_inicial.mainloop()
