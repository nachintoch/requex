#!/usr/bin/env python3

# This file is part of Requex.

# Requex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Requex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Requex.  If not, see <https://www.gnu.org/licenses/>.

import logging

import requex


logger = logging.getLogger(__name__)


class ContenidoAyuda:

    AYUDA_NO_REQUERIMIENTOS = 0
    AYUDA_LEN_GLOSARIO = 1
    AYUDA_NO_REFERENCIAS = 2
    AYUDA_RESPONSABLES_EMISION = 3
    AYUDA_VERSION = 4
    AYUDA_ALCANCES = 5

    AYUDA_TITULO_REQ = 6
    AYUDA_DESCRIPCION_REQ = 7
    AYUDA_RIESGO_REQ = 8
    AYUDA_TIPO_REQ = 9
    AYUDA_PRIORIDAD_REQ = 10
    AYUDA_VOLATILIDAD_REQ = 11
    AYUDA_ESTADO_SWEBOK_REQ = 12
    AYUDA_ESTADO_ESSENCE_REQ = 13
    AYUDA_CLAUSULA_ORG_REQ = 14
    AYUDA_DESC_CLAUSULA_ORG_REQ = 15

    AYUDA_TERMINOS_GLOSARIO = 16
    AYUDA_DEFINICION_GLOSARIO = 17

    AYUDA_REFERENCIA = 18

    HINT_CARACTERES_ESPECIALES =\
        'Éste campo acepta caracteres especiales (UTF-8). Eg: ¿ ñ ë'

    def obtener_nombre_descripcion():
        return 'REQUEX - Compilador semi-automático de Especificaciones de '\
            + 'Requerimientos'

    def obtener_descripcion_estado(instancia):
        estado = instancia.estado
        if estado == requex.Requex.ESTADO_INICIO_ANALISIS:
            return 'Extracción finalizada, por favor revise la información del'\
                + ' proyecto detectada'
        if estado == requex.Requex.ESTADO_PANTALLA_PRINCIPAL:
            if instancia.verificar_completo() == 2:
                return ('La información ingresada es completa, ahora puede '
                        + 'exportar la Especificación de Requerimientos',
                        'check.png')
            else:
                return ('Hay elementos incompletos entre los atributos de la '
                        + 'Especificación de Requerimientos', 'warning.png')
        if estado == requex.Requex.ESTADO_RECICLANDO_REQ:
            return 'Eliga un requerimiento para restaurarlo o eliminarlo de '\
                + 'forma definitiva'
        if estado in [requex.Requex.ESTADO_INSPECCION_REQ,
                      requex.Requex.ESTADO_INSPECCION_GLOSARIO,
                      requex.Requex.ESTADO_INSPECCION_RESPONSABLES,
                      requex.Requex.ESTADO_INSPECCION_REFERENCIAS]:
            return 'Agregue información ausente a la lista o seleccione una'\
                + ' entrada para modificarla'
        logger.warning('No existe ninguna descripción para el estado '
                       'identificado con {}'.format(estado))

    def obtener_descripcion_acercade():
        descripcion = ContenidoAyuda.obtener_nombre_descripcion()
        acercade = 'Versión:\t2.0.0\n\nDesarrollado por: Ignacio  "Nachintoch"'\
            + ' Castillo\nnachintoch@tutanota.com\n\nIconografía: Freepik '\
            + '(https://www.freepik.com www.flaticon.com)\t- Excepto logotipo '\
            + 'original.\n\nREQUEX no incluye NINGUNA GARANTÍA. Este es '\
            + 'software libre y puede redistribuirlo bajo ciertas condiciones.'\
            + ' Puede consultar el código fuente y la licencia en el botón al '\
            + 'final del mensaje\n\nREQUEX forma parte del proyecto de tesis '\
            + '"Herramienta de extracción de Requerimientos de Software para '\
            + 'analizar y mejorar documentación existente" (2021) para el '\
            + 'grado de Maestro en Ciencia e Ingeniería de la Computación por '\
            + 'la Universidad Nacional Autónoma de México\nConsulte la '\
            + 'propiedad intelectual al inicio de la tesis.'
        return descripcion, acercade

    def obtener_ayuda(id_atributo):
        ayuda = ContenidoAyuda.__obtener_ayuda_ers(id_atributo)
        if ayuda:
            return ayuda
        ayuda = ContenidoAyuda.__obtener_ayuda_requerimiento(id_atributo)
        if ayuda:
            return ayuda
        ayuda = ContenidoAyuda.__obtener_ayuda_glosario(id_atributo)
        if ayuda:
            return ayuda
        if id_atributo == ContenidoAyuda.AYUDA_REFERENCIA:
            return 'Indique una referencia con la que define el contenido o '\
                + 'estructura del documento en construcción '
        logger.warning('No existe ayuda para el atributo identificado por {}'
                       .format(id_atributo))

    def obtener_referencias_por_defecto():
        return [
            'I. Sommerville, Software engineering. Boston: Addison-Wesley, '
            + '2011.', '“ISO/IEC/IEEE International Standard - Systems and '
            + 'software engineering – Life cycle processes – Requirements '
            + 'engineering”,  ISOIECIEEE 291482018E, pp. 1–104, 2018.',
            'International Organization for Standardization y  International '
            + 'Electrotechnical Commission, “[ISO/IEC 15288:2008] Systems and '
            + 'software engineering —  System life cycle processes”. feb. '
            + '2008.', '“ISO/IEC/IEEE International Standard – Systems and '
            + 'software engineering - Content of life-cycle information items '
            + '(documentation)”, ISOIECIEEE 152892019E, pp. 1–86, 2019.',
            'P. Bourque, R. E. Fairley, y I. C. Society, Guide to the Software '
            + 'Engineering Body of Knowledge (SWEBOK(R)):  Version 3.0, 3rd ed.'
            + ' Washington, DC, USA: IEEE Computer Society Press, 2014.',
            'Object Management Group,  “Essence — Kernel and language for '
            + 'software engineering methods”. oct. 2018.', 'P.  Jalote, An '
            + 'Integrated Approach to Software Engineering, 3a ed. Boston, MA: '
            + 'Springer Science+Business Media, Inc., 2005.', 'R. C. Martin y '
            + 'M. Martin, Agile principles, patterns, and practices in C#. '
            + 'Upper Saddle River, NJ: Prentice Hall, 2007.']

    def obtener_ayuda_contextual(estado):  # noqa: C901
        # y aquí es donde usaría un switch
        # si python soportara uno ¬¬
        if estado in [requex.Requex.ESTADO_INICIO_ANALISIS,
                      requex.Requex.ESTADO_PANTALLA_PRINCIPAL]:
            ayuda_pantalla_principal = 'La pantalla principal presenta la '\
                + 'información general de los requerimientos extraídos y le '\
                + 'permite abrir pantallas de inspección con las que puede '\
                + 'revisar a detalle cada atributo de la Especificación de '\
                + 'Requerimientos de Software que está construyendo al usar '\
                + 'Requex.\n\nPuede usar los botones en la parte derecha de la'\
                + ' pantalla para inspeccionar los atributos descritos; '\
                + 'también puede inspeccionarlos desde la barra de menú o '\
                + 'usando los atajos de teclado asociados a cada atributo:\n\n'\
                + 'Versión - Contiene la versión de la Especificación de '\
                + 'Requerimientos que se está elaborando al usar la aplicación'\
                + ' (no la versión del software descrita por los '\
                + 'requerimientos).\nAlcances - Describe los alcances del '\
                + 'documento; puede indicar si la descripción de los '\
                + 'requerimientos se considera suficiente para continuar con '\
                + 'otros procesos de software, si se trata de un documento que'\
                + ' será revisado con los clientes, si se espera que la '\
                + 'descripción del proyecto sea incompleta; etc.\n'\
                + 'Requerimientos (F2) - Son la información central en el '\
                + 'documento, se tratan de descripciones de cualidades y '\
                + 'funciones que debe satisfacer el producto del proyecto de '\
                + 'software que se está analizando. Estas descripciones pueden'\
                + ' ser de alto nivel; siempre y cuando permitan entender y '\
                + 'reflexionar el diseño o la implementación de dicho producto'\
                + ' de software.\nGlosario (F3) - Lista de términos '\
                + 'identificados como relevantes durante la extracción de '\
                + 'requerimientos. Debe aportar una definición para los '\
                + 'términos o eliminarlos si considera que el extractor ha '\
                + 'sobreestimado la relevancia de algún término. El propósito '\
                + 'del Glosario es esclarecer el significado de términos con '\
                + 'varias posibles interpretaciones, términos en lenguajes '\
                + 'distintos, tecnicismos, siglas, abreviaciones o términos '\
                + 'propios del proyecto o de las entidades que participan en '\
                + 'él.\nResponsables de emisión (F5) - Se trata de una lista '\
                + 'en la que deben aparecer los nombres de los participantes '\
                + 'del proyecto que tienen alguna responsabilidad directa '\
                + 'sobre la producción de la Especificación de Requerimientos '\
                + 'de Software del proyecto. Estas responsabilidades pueden '\
                + 'incluir la compilación o análisis de requerimientos, '\
                + 'revisión de estilo conforme a las normas del proyecto o '\
                + 'la supervisión general de la asignación de tareas del '\
                + 'proyecto.\nReferencias (F6) - Lista de las fuentes de '\
                + 'información del proyecto contenida en el documento, '\
                + 'material de apoyo en los procesos de software del proyecto '\
                + 'o para la elaboración de la Especificación de '\
                + 'Requerimientos.\n\nTambién puede cambiar el nombre provisto'\
                + ' del proyecto (F4).\n\nAdemás de estas herramientas de '\
                + 'inspección, Requex cuenta con una Papelera de '\
                + 'Requerimientos (F7) que le permite recuperar información '\
                + 'descartada desde la lista de requerimientos.\nRequex le '\
                + 'hará saber cuando la información detectada e ingresada esté'\
                + 'completa en la parte inferior de la pantalla principal. Una'\
                + ' vez que la información en el documento esté completa, '\
                + 'podrá exportar el documento en un archivo de que podrá '\
                + 'leer, distribuir o publicar en un formato compatible con la'\
                + ' mayoría de lectores electrónicos (PDF).\n\nLas siguientes '\
                + 'son las referencias con las que se ha definido la '\
                + 'estructura y contenido de los documentos que genera Requex;'\
                + ' por lo que aparecen como las Referencias por defecto en '\
                + 'todos los documentos que se generen con esta herramienta '\
                + '(puede reemplazarlos parcial o totalmente en el inspector '\
                + 'de referencias (F6)):\n\n'
            for ref in ContenidoAyuda.obtener_referencias_por_defecto():
                ayuda_pantalla_principal += ref + '\n'
            return 'Pantalla principal - Vista del documento en construcción',\
                ayuda_pantalla_principal
        if estado == requex.Requex.ESTADO_INSPECCION_REQ:
            return 'Inspector de requerimientos', 'En la pantalla principal '\
                + 'del inpector, se encuentra la lista de requerimientos. Cada'\
                + ' entrada en la lista tiene un color de acuerdo a su estado:'\
                + '\nLas entradas de color anaranjado se encuentran '\
                + 'incompletas;\nLas entradas de color verde están completas.'\
                + '\n\nLa completitud de cada requerimiento depende de los '\
                + 'valores de sus atributos. Los requerimientos extraídos de '\
                + 'los textos analizados cuentan con título, descripción y un '\
                + 'estado por defecto (que lo señala como descrito de forma '\
                + 'muy general); pero no cuentan con atributos como riesgos, '\
                + 'prioridad, volatilidad o cláusula de organización. Así '\
                + 'mismo los requerimientos que sean agregados manualmente '\
                + 'también requieren que se propocione toda esta información.'\
                + '\n\nAl registrar o modificar un requerimiento en la lista, '\
                + 'aparecerá el editor de requerimientos; el cuál requiere '\
                + 'el ingreso de al menos tres atributos de texto libre '\
                + '(título, descripción y riesgo de cada requerimiento), el '\
                + 'resto de los valores pueden ser elegidos desde listas con '\
                + 'valores predeterminados. No es necesario utilizar todos los'\
                + 'rangos posibles de valores para describir su proyecto de '\
                + 'software; por ejemplo, puede limitar los rangos de '\
                + 'prioridad del cero al tres. Dependiendo la cláusula de '\
                + 'orgnaización que elija para el requerimiento, deberá '\
                + 'especificar con texto libre la organización; si elije '\
                + 'catalogarlos usando el tipo la descripción de la cláusula '\
                + 'es opcional.\nRequex únicamente verifica quetodos los '\
                + 'atributos cuenten con valores válidos asignados.\n\nPor '\
                + 'último, es importante recordar que los requerimientos '\
                + 'eliminados de la lista pueden ser recuperados utilizando la'\
                + ' Papelera de Requerimientos.'
        if estado == requex.Requex.ESTADO_INSPECCION_GLOSARIO:
            return 'Inspector del glosario', 'La pantalla inicial del '\
                + 'inspector muestra la lista del glosario; la cuál refleja el'\
                + ' estado de cada entrada con un color de acuerdo a su '\
                + 'estado:\nLas entradas de color anaranjado no cuentan con '\
                + 'ninguna definición.\nLas entradas de color verde están '\
                + 'completas.\n\nLas entradas del glosario consisten de dos '\
                + 'partes únicamente: el término (o varios términos sinónimos '\
                + 'a definir) y una definición.\n\nA diferencia de los '\
                + 'requerimientos, las entradas eliminadas de la lista del '\
                + 'glosario NO pueden ser recuperadas.'
        if estado == requex.Requex.ESTADO_INSPECCION_REFERENCIAS:
            return 'Inspector de referencias del documento', 'La pantalla del '\
                + 'inspector de referencias muestra una lista con las '\
                + 'referencias con las que se define el contenido de la '\
                + 'Especificación de Requerimientos en desarrollo.\n\nLas '\
                + 'referencias se limitan a texto en el que puede indicar las '\
                + 'fuentes de información con las que se sustenta el proyecto '\
                + 'de software que se describe por medio de sus requerimientos.'
        if estado == requex.Requex.ESTADO_INSPECCION_RESPONSABLES:
            return 'Inspector de responsables de emisión del documento', 'La '\
                + 'lista de responsables contiene los nombres de los '\
                + 'individuos, grupos u organizaciones que son responsables '\
                + 'por el contenido y redacción de la Especificación de '\
                + 'Requerimientos que se edita con Requex.\n\nLos responsables'\
                + ' de emisión serán indicados en la portada del documento '\
                + 'resultante, de forma que pueden ser referidos como los '\
                + 'autores del documento.'
        if estado == requex.Requex.ESTADO_RECICLANDO_REQ:
            return 'Papelera de Requerimientos', 'La papelera de '\
                + 'requerimientos muestra una lista muy similar a la que '\
                + 'aparece en el inspector de requerimientos; contiene los '\
                + 'requerimientos que han sido eliminados de la Lista de '\
                + 'Requerimientos.\nLa papelera exhibe los colores que '\
                + 'reflejan el estado de los requerimientos y permite '\
                + 'recuperar o borrar de forma definitiva los requerimientos.'
        logger.warning('No existe ayuda para el estado de la aplicación {}'
                       .format(estado))
        return None, None

    def obtener_mensaje_carga(dir_corpus, nombre_proyecto):
        return 'Buscando potenciales requerimientos de software en los '\
            + 'documentos en "' + dir_corpus + '"\nRecolectando y asociando '\
            + 'términos relevantes para el proyecto "' + nombre_proyecto + '"'\
            + '\n\nPor favor espere, esto puede tomar algunos minutos...'

    def obtener_ayuda_directorio_analisis():
        return 'Se explorará recursivamente el directorio seleccionado para '\
            + 'extraer requerimientos en los archivos que contiene. Los '\
            + 'archivos deben ser de texto plano.'

    def obtener_ayuda_nombre_proyecto():
        return 'Indique el nombre del proyecto de software a a anlizar. Este '\
            + 'nombré se usará como parte del título de la ERS resultante.\n'\
            + 'Puede modificar este valor mientras realiza el análisis'

    def obtener_descripcion_general():
        descripcion = 'Requex es una herramienta de Ingeniería de Software '\
            + 'Asistida por computadora que facilita la recuperación y '\
            + 'compilación de requerimientos de software contenidos en '\
            + 'documentos como Tesis y Reportes de Servicio Social.\n\nUtiliza'\
            + ' Procesamiento de Lenguaje Natural para el español; basado en '\
            + 'Minería de Repositorios de Software, para extraer candidatos de'\
            + ' Requerimientos de Software y términos relevantes con los que '\
            + 'se conforma un Glosario.\n\nCon esta información, prepara una '\
            + 'Especificación de Requerimientos de Software (ERS) que incluye '\
            + 'atributos sugeridos por varios estándares internacionales para '\
            + 'este tipo de documentos. Requex le presenta todos estos '\
            + 'atributos junto con el Glosario y los Requerimientos extraídos;'\
            + ' el objetivo de usar la aplicación es completar la información '\
            + 'de la ERS. Al final, podrá exportar el documento resultante.\n'\
            + '\nLa siguiente es la lista de las referencias que describen los'\
            + ' atributos que se esperan encontrar en un Requerimiento y en '\
            + 'una ERS en las que se basa la aplicación:\n'
        for ref in ContenidoAyuda.obtener_referencias_por_defecto():
            descripcion += ref + '\n'
        return descripcion

    def __obtener_ayuda_ers(id_atributo):
        if id_atributo == ContenidoAyuda.AYUDA_NO_REQUERIMIENTOS:
            return 'Cuenta de los requerimientos en la ERS en construcción'
        if id_atributo == ContenidoAyuda.AYUDA_LEN_GLOSARIO:
            return 'Cuenta de entradas del glosario de la ERS en construcción'
        if id_atributo == ContenidoAyuda.AYUDA_NO_REFERENCIAS:
            return 'Cuenta de las referencias con las que se define la ERS o '\
                + 'su contenido'
        if id_atributo == ContenidoAyuda.AYUDA_RESPONSABLES_EMISION:
            return 'Responsables de editar y aprobar la ERS en construcción'
        if id_atributo == ContenidoAyuda.AYUDA_VERSION:
            return 'Versión del documento en construcción'
        if id_atributo == ContenidoAyuda.AYUDA_ALCANCES:
            return 'Alcances del documento en construcción'
        return None

    def __obtener_ayuda_requerimiento(id_atributo):  # noqa: C901
        # ignora la complejidad ciclomática de este método, se eleva por la
        # cuenta de IFs para determinar el atributo de un requerimiento del que
        # se quiere ayuda. ¿Porqué Python no simplemente soporta un switch?
        if id_atributo == ContenidoAyuda.AYUDA_TITULO_REQ:
            return 'Proporcione un nombre corto y descriptivo al requerimiento'
        if id_atributo == ContenidoAyuda.AYUDA_DESCRIPCION_REQ:
            return 'Proporcione una descripción detallada del requerimiento'
        if id_atributo == ContenidoAyuda.AYUDA_RIESGO_REQ:
            return 'Describa los riesgos en torno al requerimiento'
        if id_atributo == ContenidoAyuda.AYUDA_TIPO_REQ:
            return 'De acuerdo al ISO/IEC/IEEE 29148:2018:\nFuncional - Tarea '\
                + 'que debe desempeñar el software\nInterfaz - Característica '\
                + 'que debe cumplir para interactuar con otro sistema, '\
                + 'componente o usuarios\nCalidad o No funcional - Cualidad o '\
                + 'restricción que debe satisfacer el software\nUsabilidad - '\
                + 'Consideración para atender las necesidades de los '\
                + 'usuarios\nFactor humano - Consideración en favor de '\
                + 'limitaciones, salud y condiciones humanas'
        if id_atributo == ContenidoAyuda.AYUDA_PRIORIDAD_REQ:
            return 'Cero representa los requerimientos de mayor prioridad'
        if id_atributo == ContenidoAyuda.AYUDA_VOLATILIDAD_REQ:
            return 'Cero representa estabilidad total en la definición del '\
                + 'requerimiento, diez indica que se esperan cambios o '\
                + 'suspensión del requerimiento'
        if id_atributo == ContenidoAyuda.AYUDA_ESTADO_SWEBOK_REQ:
            return 'Estados recomendados por SWEBOK:\nEn progreso - El '\
                + 'requerimiento se encuentra en una etapa temprana de '\
                + 'desarrollo\nEn revisión - La definición o implementación '\
                + 'del requerimiento se encuentra en revisión\nSuspendido - El'\
                + ' requerimiento ha sido abandonado o pertenece a una versión'\
                + ' obsoleta\nTerminado - El requerimiento ha sido '\
                + 'implementado, revisado y aprobado\nPuede combinar estos '\
                + 'estados con los recomendados por Essence'
        if id_atributo == ContenidoAyuda.AYUDA_ESTADO_ESSENCE_REQ:
            return 'Estados recomendados por OMG - Essence:\nIdentificado - El'\
                + ' requerimiento ha sido descrito en un alto nivel\nDescrito '\
                + '- El requerimiento ha sido descrito de forma clara, '\
                + 'concisa, completa, consistente y verificable\nImplementado '\
                + '- El requerimiento ha sido implementado\nVerificado - Se ha'\
                + ' confirmado una exitosa implementación del '\
                + 'requerimiento\nPuede combinar estos estados con los '\
                + 'recomendados por SWEBOK'
        if id_atributo == ContenidoAyuda.AYUDA_CLAUSULA_ORG_REQ:
            return 'Se usa para agrupar y organizar los requerimientos en el '\
                + 'documento ERS resultante'
        if id_atributo == ContenidoAyuda.AYUDA_DESC_CLAUSULA_ORG_REQ:
            return 'Especificación personalizada para la agrupación de los '\
                + 'requerimientos en el documento resultante'
        return None

    def __obtener_ayuda_glosario(id_atributo):
        if id_atributo == ContenidoAyuda.AYUDA_TERMINOS_GLOSARIO:
            return 'Indique el término a definir; puede incluir sus sinónimos '\
                + 'separando los términos con coma'
        if id_atributo == ContenidoAyuda.AYUDA_DEFINICION_GLOSARIO:
            return 'Describa los términos que se definen en esta entrada del '\
                + 'glosario'
        return None
