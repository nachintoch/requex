# This file is part of Requex.

# Requex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Requex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Requex.  If not, see <https://www.gnu.org/licenses/>.

from fpdf import FPDF
from requex.modelos import EspecificacionRequerimientosSoftware
from requex.modelos import Requerimiento

import locale
import logging
logger = logging.getLogger(__name__)


class ExportadorErs:

    def __init__(self, ers):
        self._ers = ers

    def exportar(self, archivo_destino):
        self._primera_pagina()
        self._encabezados_pie_pagina()
        self._alcances()
        self._requerimientos()
        self._glosario()
        self._referencias()
        self._tabla_contenidos()
        self._escribir_documento(archivo_destino)

    def _primera_pagina(self):
        pass

    def _tabla_contenidos(self):
        pass

    def _encabezados_pie_pagina(self):
        pass

    def _alcances(self):
        pass

    def _requerimientos(self):
        pass

    def _glosario(self):
        pass

    def _referencias(self):
        pass

    def _escribir_documento(self, archivo):
        pass

    def registrar_formato():
        raise NotImplementedError('El exportador base no puede ser registrado '
                                  + 'para generar archivos ERS. Asegúrese de '
                                  + 'sobre-escribir este método')

    def formatear_clausula_organizacion(clausula_org):
        clausula, org_especifica = clausula_org
        if clausula in Requerimiento.obtener_tipos_validos():
            if 'funcional' in clausula.lower():
                clausula += 'es'
            else:
                clausula = 'de ' + clausula
        else:
            clausula = 'de ' + clausula
        clausula_org = 'Requerimientos {}'.format(clausula)
        if org_especifica:
            clausula_org += ' - ' + org_especifica
        return clausula_org

    def formatear_estado_requerimiento(estado):
        texto_estado = 'Estado: {}'
        if estado[0] is None:
            return texto_estado.format(estado[1])
        if estado[1] is None:
            return texto_estado.format(estado[0])
        return texto_estado.format(estado[0] + ', ' + estado[1])


class ExportadorErsPdf(ExportadorErs):

    def __init__(self, ers):
        super().__init__(ers)
        try:
            locale.setlocale(locale.LC_TIME, 'es_MX.utf8')
        except locale.Error:
            logger.error('No se ha podido configurar el exportador en español')
        self.__fecha = self._ers.fecha_emision.strftime('%d de %B del %Y')
        estado = EspecificacionRequerimientosSoftware.obtener_estados_validos()
        self.__estado = estado[self._ers.verificar_completo()]
        self.__doc = ExportadorErsPdf.__FormatoErsPdf(
            ers.titulo, ers.version, self.__fecha, self.__estado)

    def _primera_pagina(self):
        self.__doc._agregar_encabezados = False
        self.__doc.add_page()
        self.__doc.set_font('Helvetica', size=18)
        titulo = ExportadorErsPdf.__codifica_texto(self._ers.titulo)
        self.__doc.multi_cell(175, 10, titulo)
        self.__doc.set_font('Helvetica', size=12)
        self.__doc.cell(0, 35, ln=1)
        self.__doc.cell(150, 10, 'Responsables de emisión:', align='R', ln=1)
        for responsable in self._ers.responsables_emision.values():
            self.__doc.cell(150, 10, responsable, align='R', ln=1)
        self.__doc.cell(0, 80, ln=1)
        self.__doc.cell(150, 10, 'Versión del documento: '
                        + self._ers.version, ln=1)
        self.__doc.cell(150, 10, 'Fecha de emisión: ' + self.__fecha, ln=1)
        self.__doc.cell(150, 10, 'Estado del documento: ' + self.__estado, ln=1)

    def _alcances(self):
        self.__registra_seccion_documento('Alcances')
        self.__doc.set_font('Helvetica', size=12)
        alcances = ExportadorErsPdf.__codifica_texto(self._ers.alcances)
        self.__doc.multi_cell(200, 5, alcances)
        self.__doc.ln(3)

    def _requerimientos(self):
        self.__registra_seccion_documento('Requerimientos')
        tabla_contenidos = self._ers.tabla_contenidos.items()
        for clausula_org, id_requerimientos in tabla_contenidos:
            if clausula_org:
                titulo_seccion = ExportadorErs.formatear_clausula_organizacion(
                    clausula_org)
            else:
                titulo_seccion = 'Requerimientos no categorizados'
            self.__doc._titulo_seccion = titulo_seccion
            self.__doc.registrar_entrada_tabla_contenidos(titulo_seccion, 1)
            self.__doc.set_font('Helvetica', size=16)
            self.__doc.cell(200, 20, titulo_seccion, align='L', ln=1)
            id_requerimientos.sort()
            for id in id_requerimientos:
                requerimiento = self._ers.requerimientos[id]
                self.__doc.set_font('Helvetica', style='B', size=12)
                titulo = ExportadorErsPdf.__codifica_texto(requerimiento.titulo)
                self.__doc.multi_cell(200, 6, '{} - {}. Prioridad: {}'.format(
                    id, titulo, requerimiento.prioridad))
                self.__doc.ln(5)
                self.__doc.set_font('Helvetica', size=12)
                descripcion = ExportadorErsPdf.__codifica_texto(
                    requerimiento.descripcion)
                self.__doc.multi_cell(200, 5, descripcion)
                self.__doc.ln(3)
                estado = ExportadorErs.formatear_estado_requerimiento(
                    requerimiento.estado)
                self.__doc.cell(80, 5, txt=estado, align='L')
                self.__doc.cell(40, 5, txt='Volatilidad: {}'.format(
                                requerimiento.volatilidad), align='C', ln=1)
                riesgo = ExportadorErsPdf.__codifica_texto(requerimiento.riesgo)
                self.__doc.multi_cell(200, 5, 'Riesgo: {}'.format(riesgo))
                self.__doc.ln(15)

    def _glosario(self):
        self.__registra_seccion_documento('Glosario')
        glosario = list(self._ers.glosario.values())
        glosario.sort()
        for entrada in glosario:
            terminos = list(entrada.terminos)
            terminos.sort(reverse=True)
            terms = ', '.join(terminos)
            definicion = entrada.definicion
            self.__doc.set_font('Helvetica', style='B', size=12)
            terms = ExportadorErsPdf.__codifica_texto(terms)
            self.__doc.cell(200, 6, terms, align='L', ln=1)
            self.__doc.set_font('Helvetica', size=12)
            definicion = ExportadorErsPdf.__codifica_texto(definicion)
            self.__doc.cell(200, 5, definicion, align='J', ln=1)
            self.__doc.ln(3)

    def _referencias(self):
        self.__registra_seccion_documento('Referencias')
        referencias = [ref for ref in self._ers.referencias.values()]
        referencias.sort()
        self.__doc.set_font('Helvetica', size=12)
        for referencia in referencias:
            ref = ExportadorErsPdf.__codifica_texto(referencia, 'ignore')
            self.__doc.multi_cell(200, 5, ref)
            self.__doc.ln(3)

    def _tabla_contenidos(self):
        self.__doc.crear_tabla_contenidos()

    def _escribir_documento(self, archivo):
        self.__doc.output(archivo)

    def __registra_seccion_documento(self, titulo):
        self.__doc._agregar_encabezados = True
        self.__doc._titulo_seccion = titulo
        self.__doc.add_page()
        self.__doc.registrar_entrada_tabla_contenidos(titulo)

    def registrar_formato():
        return ('Formato de Documento Portable', '*.pdf')

    def __codifica_texto(texto, accion='replace'):
        if not texto:
            return ''
        text = texto.encode('latin-1', accion)
        return text.decode('latin-1')

    class __FormatoErsPdf(FPDF):

        def __init__(self, titulo_doc, version_doc, fecha, estado_doc):
            super().__init__(format='letter')
            self._titulo_seccion = None
            self._agregar_encabezados = False
            self.__tabla_contenidos = []
            self.__titulo_documento = titulo_doc
            self.__version_documento = version_doc
            self.__fecha_documento = fecha
            self.__estado_documento = estado_doc

        def header(self):
            if not self._agregar_encabezados:
                return
            self.set_font('Helvetica', style='B', size=12)
            self.cell(20, 10, self.__titulo_documento, align='L', ln=1)
            self.cell(175, 10, self._titulo_seccion, align='R', ln=1)
            self.ln(5)

        def footer(self):
            # TODO esta condición no se compoarta como lo esperado, quizá sea
            # mejor primero calcular el número de página y si es la primera,
            # entonces se omite el pie de página
            if not self._agregar_encabezados:
                return
            self.set_font('Helvetica', size=9)
            self.set_y(-20)
            self.cell(20, 5, 'Versión del documento: '
                      + self.__version_documento, align='L')
            self.cell(175, 5, 'Fecha de emisión: ' + self.__fecha_documento,
                      align='R', ln=1)
            self.cell(20, 10, 'Estado del documento: '
                      + self.__estado_documento, align='L')
            pagina = 'Página: ' + str(self.page_no()) + '/{nb}'
            self.cell(175, 10, pagina, align='R')

        def registrar_entrada_tabla_contenidos(self, titulo, nivel=0):
            self.__tabla_contenidos.append({
                'txt': titulo,
                'nivel': nivel,
                'pag': str(self.page_no())
                })

        def crear_tabla_contenidos(self):
            self._titulo_seccion = 'Tabla de contenidos'
            self.add_page()
            self.set_font('Helvetica', size=12)
            for entrada in self.__tabla_contenidos:
                nivel = entrada['nivel']
                if nivel > 0:
                    self.cell(nivel * 8)
                texto = entrada['txt']
                ancho_texto = self.get_string_width(texto) + 2
                self.cell(ancho_texto, 8, texto)
                pagno = entrada['pag']
                ancho_pagno = self.get_string_width(pagno) + 2
                ancho_relleno = 200 - ancho_pagno - (nivel * 8)
                ancho_relleno -= ancho_texto + 2
                num_puntos = ancho_relleno / self.get_string_width('.')
                puntos = '.' * int(num_puntos)
                self.cell(ancho_relleno, 8, puntos, align='R')
                self.cell(ancho_pagno, 8, pagno, align='R', ln=1)
