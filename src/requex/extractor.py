# This file is part of Requex.

# Requex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Requex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Requex.  If not, see <https://www.gnu.org/licenses/>.

import difflib
import json
import logging
import numpy as np
import os
import re

import es_core_news_md
from bs4 import BeautifulSoup

from gensim.corpora import Dictionary
from gensim.models.phrases import Phrases, Phraser
from gensim.models.word2vec import Word2Vec
from gensim.models import KeyedVectors
from gensim.models import TfidfModel

import requex
from requex.modelos import Requerimiento
from requex.modelos import Glosario
from requex.modelos import Colocacion


logger = logging.getLogger(__name__)
logger_extraccion_req = logging.getLogger('{}.REQUERIMIENTOS'.format(__name__))
logger_extraccion_terms = logging.getLogger('{}.TERMINOS'.format(__name__))


class MineroRequerimientos:

    archivo_modelo_ngramas = os.sep.join(['res', 'modelos', 'ngramas.mod'])

    archivo_modelo_diccionario = os.sep.join(['res', 'modelos',
                                              'diccionario.mod'])

    archivo_mod_vect_sim = os.sep.join(['res', 'modelos',
                                        'vectores_similitud.mod'])

    archivo_mod_vect_rel = os.sep.join(['res', 'modelos',
                                        'vectores_relevancia.mod'])

    archivo_modelo_sustantivos_clave = os.sep.join(['res', 'modelos',
                                                    'sustantivos_clave.mod'])

    archivo_modelo_verbos_clave = os.sep.join(['res', 'modelos',
                                               'verbos_clave.mod'])

    archivo_modelo_perifrasis_verbal = os.sep.join(['res', 'modelos',
                                                    'perifrasis_verbal.mod'])

    patron_genero = re.compile(r'Gender=\w+')
    patron_numero = re.compile(r'Number=\w+')
    patron_url = re.compile(
        r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)"
        + r"(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|"
        + r"(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))")
    patron_interrogativo = re.compile(r'\w+\? |\?$')
    patron_codigo = re.compile(
        r'(?:\w+\d* ?)(?:\.\w+\d* ?)*(?:\(.*\)|'
        + r'[+\-/*!=]?= ?(?:["\']?\w*["\']|\d+))')

    def __init__(self, relevancia_min=75):
        if relevancia_min < 0 or relevancia_min > 100:
            raise AttributeError('La relevancia mínima debe ser un número '
                                 + 'entre 0 y 100')
        self.__procesador_ln = es_core_news_md.load()
        self.__modelo_ngramas = None
        self.__modelo_diccionario = None
        self.__modelo_vectores_sim = None
        self.__modelo_vectores_rel = None
        self.__sustantivos_clave = []
        self.__verbos_clave = []
        self.__verbos_perifrasis = {}
        self.__relevancia_min = relevancia_min / 100
        self.__inicializar_palabras_vacias()
        self.__cargar_modelos()

    def entrenar(self, corpus, congelar_modelos=False, guardar_modelos=True,
                 freq_min=3):
        corpus_entramiento = []
        for doc in corpus:
            oraciones_etiquetadas = self.__preprocesar(doc)
            corpus_entramiento +=\
                self.__filtrar_palabras_vacias(oraciones_etiquetadas)
        sustantivos_clave = self.__preprocesar(' '.join(
            MineroRequerimientos.obtener_sustantivos_clave()))
        verbos_clave = self.__preprocesar(' '.join(
            MineroRequerimientos.obtener_verbos_clave()))
        verbos_perifrasis = self.__preprocesar(' '.join(
            MineroRequerimientos.obtener_verbos_perifrasis()))
        self.__prepara_modelos(corpus_entramiento, freq_min)
        MineroRequerimientos.__extraer_palabras_clave_preprocesadas(
            self.__sustantivos_clave, sustantivos_clave)
        MineroRequerimientos.__extraer_palabras_clave_preprocesadas(
            self.__verbos_clave, verbos_clave)
        MineroRequerimientos.__extraer_palabras_clave_preprocesadas(
            self.__verbos_perifrasis, verbos_perifrasis, True)
        if guardar_modelos:
            self.__guardar_modelos(congelar_modelos)

    def extraer_requerimientos(self, corpus, congelar_modelos=False):
        self.entrenar(corpus, congelar_modelos, False)
        requerimientos_extraidos = set()
        glosario_extraido = set()
        for doc in corpus:
            oraciones_etiquetadas = self.__preprocesar(doc)
            oraciones_filtradas =\
                self.__filtrar_palabras_vacias(oraciones_etiquetadas)
            vectores_similitud = self.__vectorizar_similitud(
                oraciones_filtradas)
            vectores_relevancia = self.__vectorizar_relevancia(
                oraciones_filtradas)
            colocaciones = self.__detectar_colocaciones(oraciones_filtradas,
                                                        vectores_similitud)
            requerimientos, glosario = self.__minar(
                oraciones_etiquetadas, colocaciones, vectores_relevancia)
            requerimientos_extraidos.update(requerimientos)
            glosario_extraido.update(glosario)
        glosario = Glosario()
        for termino in glosario_extraido:
            glosario.agregar_entrada(termino)
        return requerimientos_extraidos, glosario_extraido

    def __prepara_modelos(self, corpus_entramiento, freq_min):
        # TODO la elección de estas variables es muy importante. Hay que
        # revisar si los resultados se adecuan a estros u otros valores
        # hardcodeadeados. En otro caso parametrizar.
        # En todo caso, documentar las desiciones al rededor de estas variables
        # Phrases  - https://radimrehurek.com/gensim/models/phrases.html
        # Word2Vec - https://radimrehurek.com/gensim/models/word2vec.html
        if not self.__modelo_ngramas:
            self.__modelo_ngramas = Phrases(
                corpus_entramiento, min_count=freq_min, threshold=1)
        else:
            self.__modelo_ngramas.add_vocab(corpus_entramiento)
        if not self.__modelo_vectores_sim:
            self.__modelo_vectores_sim = Word2Vec(
                corpus_entramiento, size=160, window=5, min_count=freq_min,
                sample=0, workers=6, sg=1, negative=5, iter=20,
                compute_loss=True)
        else:
            self.__modelo_vectores_sim.build_vocab(corpus_entramiento,
                                                   update=True)
            self.__modelo_vectores_sim.train(
                corpus_entramiento,
                total_examples=self.__modelo_vectores_sim.corpus_count,
                epochs=15)
        if not self.__modelo_diccionario:
            self.__modelo_diccionario = Dictionary(corpus_entramiento)
        else:
            self.__modelo_diccionario.add_documents(corpus_entramiento)
        self.__prepara_modelo_tfidf(corpus_entramiento)

    def __prepara_modelo_tfidf(self, corpus_entramiento):
        bolsa_palabras = [self.__modelo_diccionario.doc2bow(doc) for doc
                          in corpus_entramiento]
        tfidf = TfidfModel(bolsa_palabras, id2word=self.__modelo_diccionario,
                           smartirs='ntc')
        for oracion_tfidf in tfidf[bolsa_palabras]:
            for id, token_tfidf in oracion_tfidf:
                self.__modelo_vectores_rel[self.__modelo_diccionario[id]] =\
                    token_tfidf

    def __cargar_modelos(self):
        self.__carga_modelos_base()
        self.__carga_modelo_vectores_sim()
        self.__carga_modelo_vectores_rel()
        self.__carga_modelo_sustantivos()
        self.__carga_modelos_verbales()

    def __carga_modelo_vectores_sim(self):
        try:
            self.__modelo_vectores_sim =\
                Word2Vec.load(MineroRequerimientos.archivo_mod_vect_sim)
        except Exception:
            try:
                self.__modelo_vectores_sim = KeyedVectors.load(
                    MineroRequerimientos.archivo_mod_vect_sim)
            except Exception as e:
                logger.debug('No se pudo abrir el archivo %s - %s',
                             MineroRequerimientos.archivo_mod_vect_sim, e)

    def __carga_modelo_vectores_rel(self):
        try:
            with open(MineroRequerimientos.archivo_mod_vect_rel,
                      'r') as archivo_vectores:
                modelo_vectores = archivo_vectores.read()
            self.__modelo_vectores_rel = json.loads(modelo_vectores)
        except Exception as e:
            self.__modelo_vectores_rel = {}
            logger.debug('No se pudo abrir el archivo %s - %s',
                         MineroRequerimientos.archivo_mod_vect_rel, e)

    def __carga_modelos_base(self):
        try:
            self.__modelo_ngramas =\
                Phrases.load(MineroRequerimientos.archivo_modelo_ngramas)
        except Exception as e:
            logger.debug('No se pudo abrir el archivo %s - %s',
                         MineroRequerimientos.archivo_modelo_ngramas, e)
        try:
            self.__modelo_diccionario =\
                Dictionary.load(
                    MineroRequerimientos.archivo_modelo_diccionario)
        except Exception as e:
            logger.debug('No se pudo abrir el archivo %s - %s',
                         MineroRequerimientos.archivo_modelo_diccionario, e)

    def __carga_modelo_sustantivos(self):
        try:
            with open(MineroRequerimientos.archivo_modelo_sustantivos_clave,
                      'r') as archivo_sustantivos:
                modelo_sustantivos = archivo_sustantivos.read()
            self.__sustantivos_clave = json.loads(modelo_sustantivos)
        except Exception as e:
            logger.debug('No se pudo abrir el archivo %s - %s',
                         MineroRequerimientos.archivo_modelo_sustantivos_clave,
                         e)

    def __carga_modelos_verbales(self):
        try:
            with open(MineroRequerimientos.archivo_modelo_verbos_clave,
                      'r') as archivo_verbos:
                modelo_verbos = archivo_verbos.read()
            self.__verbos_clave = json.loads(modelo_verbos)
        except Exception as e:
            logger.debug('No se pudo abrir el archivo %s - %s',
                         MineroRequerimientos.archivo_modelo_verbos_clave,
                         e)
        try:
            with open(MineroRequerimientos.archivo_modelo_perifrasis_verbal,
                      'r') as archivo_verbos:
                modelo_perifrasis = archivo_verbos.read()
            self.__verbos_perifrasis = json.loads(modelo_perifrasis)
        except Exception as e:
            logger.debug('No se pudo abrir el archivo %s - %s',
                         MineroRequerimientos.archivo_modelo_perifrasis_verbal,
                         e)

    def __guardar_modelos(self, congelar_modelos):
        if congelar_modelos:
            self.__modelo_ngramas = Phraser(self.__modelo_ngramas)
            self.__modelo_vectores_sim = self.__modelo_vectores_sim.wv
        self.__modelo_ngramas.save(MineroRequerimientos.archivo_modelo_ngramas)
        self.__modelo_diccionario.save(
            MineroRequerimientos.archivo_modelo_diccionario)
        self.__modelo_vectores_sim.save(
            MineroRequerimientos.archivo_mod_vect_sim)
        with open(MineroRequerimientos.archivo_mod_vect_rel,
                  'w') as archivo_vectores:
            archivo_vectores.write(json.dumps(self.__modelo_vectores_rel))
        with open(MineroRequerimientos.archivo_modelo_sustantivos_clave,
                  'w') as archivo_sustantivos:
            archivo_sustantivos.write(json.dumps(self.__sustantivos_clave))
        with open(MineroRequerimientos.archivo_modelo_verbos_clave,
                  'w') as archivo_verbos:
            archivo_verbos.write(json.dumps(self.__verbos_clave))
        with open(MineroRequerimientos.archivo_modelo_perifrasis_verbal,
                  'w') as archivo_verbos:
            archivo_verbos.write(json.dumps(self.__verbos_perifrasis))

    def __preprocesar(self, documento):
        documento_etiquetado = self.__procesador_ln(documento.lower())
        return [oracion for oracion in documento_etiquetado.sents]

    def __vectorizar_similitud(self, oraciones_filtradas):
        vectores = {}
        for oracion in oraciones_filtradas:
            for term in oracion:
                try:
                    vectores[term] = self.__modelo_vectores_sim[term]
                except KeyError:
                    vectores[term] = np.array([])
        return vectores

    def __vectorizar_relevancia(self, oraciones_filtradas):
        vectores = {}
        for oracion in oraciones_filtradas:
            for term in oracion:
                try:
                    vectores[term] = self.__modelo_vectores_rel[term]
                except KeyError:
                    raise RuntimeError('El entranamiento de'
                                       + ' MineroRequerimientos es '
                                       + 'insuficiente para procesar el '
                                       + 'corpus. Entrene MineroRequerimientos'
                                       + ' con el corpus dado; de ser posible '
                                       + 'agregue otros documentos similares '
                                       + 'para el entranmiento')
        return vectores

    def __detectar_colocaciones(self, oraciones_filtradas, vectores_similitud):
        colocaciones = set()
        for oracion in oraciones_filtradas:
            ultima_deteccion = oracion
            continuar_generando = True
            while continuar_generando:
                nueva_deteccion = self.__modelo_ngramas[ultima_deteccion]
                if len(nueva_deteccion) == len(ultima_deteccion):
                    break
                ultima_deteccion = nueva_deteccion
                self.__busca_agrega_colocacion(ultima_deteccion, colocaciones,
                                               vectores_similitud)
        return colocaciones

    def __busca_agrega_colocacion(self, oracion, colocaciones,
                                  vectores_similitud):
        for token in oracion:
            if token.find('_') and token not in colocaciones:
                colocacion_agrupar = Colocacion(token)
                for termino in colocaciones:
                    if self.__es_colocacion_relevante(colocacion_agrupar,
                                                      termino,
                                                      vectores_similitud):
                        colocaciones.add(colocacion_agrupar)
                    else:
                        break

    # TODO este umbral quiza deba ser parametrizado
    def __es_colocacion_relevante(self, colocacion_agrupar, termino,
                                  vectores_similitud, sim_min=0.85):
        relevante = True
        for palabra in colocacion_agrupar:
            for p in termino:
                if vectores_similitud.similarity(p, palabra) < sim_min:
                    relevante = False
                    break
        return relevante

    def __minar(self, oraciones_etiquetadas, colocaciones,
                vectores_relevancia):
        requerimientos = set()
        glosario = set()
        for oracion in oraciones_etiquetadas:
            titulo = ''
            descripcion = ''
            terminos_encontrados = []
            es_patron_valido = False
            ultima_categoria_detectada = None
            completar_titulo = True
            verbo_compuesto = None
            indice_primer_token = 0
            contar_tokens = True
            for token in oracion:
                if contar_tokens:
                    indice_primer_token += 1
                if token.is_space:
                    continue
                titulo, descripcion, es_patron_valido,\
                    ultima_categoria_detectada, completar_titulo,\
                    verbo_compuesto, terminos_encontrados =\
                    self.__extrae_requerimiento(
                        titulo, descripcion, token, es_patron_valido,
                        ultima_categoria_detectada, colocaciones,
                        completar_titulo, verbo_compuesto,
                        terminos_encontrados)
                if titulo:
                    contar_tokens = False
            self.__agregar_terminos_glosario(glosario, terminos_encontrados,
                                             vectores_relevancia)
            titulo = self.__limpiar_texto(titulo)
            descripcion = self.__limpiar_texto(descripcion)
            titulo = requex.Requex.formatea_texto(titulo)
            descripcion = requex.Requex.formatea_texto(titulo)
            if not titulo or indice_primer_token > len(oracion) * 0.65 or\
                    not self.__es_descripcion_valida(descripcion):
                continue
            self.__agregar_requerimiento(requerimientos, titulo, descripcion)
        self.__registra_informacion_extraida(requerimientos, glosario)
        return requerimientos, glosario

    def __extrae_requerimiento(self, titulo, descripcion, token,
                               es_patron_valido, ultima_categoria_detectada,
                               colocaciones, completar_titulo, verbo_compuesto,
                               terminos_encontrados):
        pronombre_anterior, es_patron_valido, completar_titulo =\
            self.__identifica_extrae_texto_patron(
                token, es_patron_valido, ultima_categoria_detectada,
                colocaciones, completar_titulo, verbo_compuesto)
        if not es_patron_valido:
            return titulo, descripcion, es_patron_valido,\
                ultima_categoria_detectada, completar_titulo, verbo_compuesto,\
                terminos_encontrados
        titulo, descripcion = self.__completa_textos(
            token, completar_titulo, pronombre_anterior, titulo, descripcion)
        self.__prepara_terminos_glosario(token, terminos_encontrados,
                                         colocaciones)
        return titulo, descripcion, es_patron_valido,\
            ultima_categoria_detectada, completar_titulo, verbo_compuesto,\
            terminos_encontrados

    # (SUST*+VERB*+ADJ*+ADV*)+
    def __identifica_extrae_texto_patron(self, token, es_patron_valido,
                                         ultima_categoria_detectada,
                                         colocaciones, completar_titulo,
                                         verbo_compuesto):
        pronombre_anterior = ''
        if token.pos_ == 'NOUN':
            es_patron_valido, ultima_categoria_detectada, completar_titulo,\
                pronombre_anterior, verbo_compuesto =\
                self.__detecta_repeticion_patron(token, es_patron_valido,
                                                 ultima_categoria_detectada,
                                                 completar_titulo,
                                                 pronombre_anterior,
                                                 verbo_compuesto)
        elif token.pos_ == 'VERB':
            es_patron_valido, ultima_categoria_detectada, verbo_compuesto,\
                completar_titulo, pronombre_anterior =\
                self.__detecta_patron_verbal(token, es_patron_valido,
                                             ultima_categoria_detectada,
                                             colocaciones, verbo_compuesto,
                                             completar_titulo,
                                             pronombre_anterior)
        elif es_patron_valido:
            self.__detecta_fin_patron(token, ultima_categoria_detectada,
                                      completar_titulo)
        return pronombre_anterior, es_patron_valido, completar_titulo

    def __detecta_repeticion_patron(self, token, es_patron_valido,
                                    ultima_categoria_detectada,
                                    completar_titulo, pronombre_anterior,
                                    verbo_compuesto):
        if token.lemma_ in self.__sustantivos_clave:
            es_patron_valido = True
        if es_patron_valido:
            if ultima_categoria_detectada and\
                    ultima_categoria_detectada != 'NOUN':
                completar_titulo = False
            if not ultima_categoria_detectada:
                pronombre_anterior =\
                    self.__obtener_pronombre_anterior(token)
            ultima_categoria_detectada = 'NOUN'
            verbo_compuesto = token.i
        return es_patron_valido, ultima_categoria_detectada, completar_titulo,\
            pronombre_anterior, verbo_compuesto

    def __detecta_patron_verbal(self, token, es_patron_valido,
                                ultima_categoria_detectada, colocaciones,
                                verbo_compuesto, completar_titulo,
                                pronombre_anterior):
        if token.lemma_ in self.__verbos_clave:
            es_patron_valido = True
        elif not es_patron_valido and token.lemma_ in self.__verbos_perifrasis\
                and self.__detectar_perifrasis_verbal(token):
            es_patron_valido = True
        if es_patron_valido:
            completar_titulo, pronombre_anterior, ultima_categoria_detectada,\
                verbo_compuesto = self.__detecta_posicion_patron_verbal(
                    token, completar_titulo, ultima_categoria_detectada,
                    pronombre_anterior, verbo_compuesto, colocaciones)
        return es_patron_valido, ultima_categoria_detectada, verbo_compuesto,\
            completar_titulo, pronombre_anterior

    def __detecta_posicion_patron_verbal(self, token, completar_titulo,
                                         ultima_categoria_detectada,
                                         pronombre_anterior, verbo_compuesto,
                                         colocaciones):
        if ultima_categoria_detectada in ['NOUN', 'VERB']:
            token_anterior = token.doc[verbo_compuesto]
            if MineroRequerimientos.patron_genero.findall(
                    token_anterior.tag_) ==\
                    MineroRequerimientos.patron_genero.findall(token.tag_) and\
                    MineroRequerimientos.patron_numero.findall(
                        token_anterior.tag_) ==\
                    MineroRequerimientos.patron_numero.findall(token.tag_):
                colocaciones.add(Colocacion(
                    str(token.doc[verbo_compuesto:token.i]), ' '))
        elif ultima_categoria_detectada in ['ADJ', 'ADV']:
            completar_titulo = False
        if not ultima_categoria_detectada:
            pronombre_anterior = self.__obtener_pronombre_anterior(token)
        ultima_categoria_detectada = 'VERB'
        verbo_compuesto = token.i
        return completar_titulo, pronombre_anterior,\
            ultima_categoria_detectada, verbo_compuesto

    def __detecta_fin_patron(self, token, ultima_categoria_detectada,
                             completar_titulo):
        if token.pos_ == 'ADJ':
            if ultima_categoria_detectada == 'ADV':
                completar_titulo = False
            ultima_categoria_detectada = 'ADJ'
        elif token.pos_ == 'ADV':
            ultima_categoria_detectada = 'ADV'
        return ultima_categoria_detectada, completar_titulo

    def __completa_textos(self, token, completar_titulo, pronombre_anterior,
                          titulo, descripcion):
        if completar_titulo:
            if not titulo:
                titulo = pronombre_anterior
                descripcion = pronombre_anterior
            if (not token.is_punct and
                    not token.doc[token.i - 1].is_left_punct) or\
                    (token.is_left_punct and
                     not token.doc[token.i - 1].is_left_punct):
                titulo += ' '
            titulo += token.text
        if (not token.is_punct and not token.doc[token.i - 1].is_left_punct) or\
                (token.is_left_punct and
                 not token.doc[token.i - 1].is_left_punct):
            descripcion += ' '
        descripcion += token.text
        return titulo, descripcion

    def __prepara_terminos_glosario(self, token, terminos_encontrados,
                                    colocaciones):
        if token.lemma_ not in terminos_encontrados and not token.is_stop and\
                not token.is_punct and not token.like_num and\
                not token.like_url and not token.like_email and\
                len(token.lemma_) >= 2:
            if token.lemma_ in colocaciones:
                terminos_encontrados +=\
                    Colocacion.colocaciones_con_termino(colocaciones,
                                                        token.lemma_)
            else:
                terminos_encontrados.append(Colocacion(token.lemma_))

    def __agregar_terminos_glosario(self, glosario, terminos_encontrados,
                                    vectores_relevancia):
        for colocacion in terminos_encontrados:
            for term in colocacion:
                term = str(term)
                if term not in vectores_relevancia or\
                        vectores_relevancia[term] >= self.__relevancia_min:
                    colocacion.formato = ' '
                    termino = str(colocacion)
                    glosario.add(termino)
                    break

    def __agregar_requerimiento(self, requerimientos, titulo, descripcion):
        estado = 'Identificado'
        try:
            id = len(requerimientos) + 1
            requerimiento = Requerimiento(id, titulo, descripcion, estado)
            if self.__es_requerimiento_unico(requerimiento,
                                             requerimientos):
                requerimientos.add(requerimiento)
        except AttributeError:
            pass

    def __obtener_pronombre_anterior(self, token):
        posicion_anterior = token.i - 1
        while posicion_anterior >= 0:
            if token.doc[posicion_anterior].is_space:
                posicion_anterior -= 1
            elif token.doc[posicion_anterior].pos_ == 'PRON':
                return token.doc[posicion_anterior].text
            else:
                return ''

    def __obtener_palabra_siguiente(self, token, sintagma=False):
        posicion_siguiente = token.i + 1
        while posicion_siguiente < len(token.doc):
            if token.doc[posicion_siguiente].is_space:
                posicion_siguiente += 1
            else:
                return token.doc[posicion_siguiente].pos_ if sintagma else\
                    token.doc[posicion_siguiente].text

    def __detectar_perifrasis_verbal(self, token):
        verbo = self.__verbos_perifrasis[token.lemma_]
        if verbo == 'ir' and 'Pres' in token.tag_:
            return self.__obtener_palabra_siguiente(token) == 'a'
        if verbo == 'tener':
            return self.__obtener_palabra_siguiente(token) == 'que'
        if verbo == 'haber' and 'Person=3' in token.tag_:
            return self.__obtener_palabra_siguiente(token) == 'que'
        if verbo == 'estar':
            return self.__obtener_palabra_siguiente(token, sintagma=True)\
                == 'VERB'
        return False

    def __inicializar_palabras_vacias(self):
        to_remove = set()
        for palabra_vacia in self.__procesador_ln.Defaults.stop_words:
            if (palabra_vacia.startswith('un') and len(palabra_vacia) <= 4) or\
                    palabra_vacia in ['dos', 'tres', 'cuatro', 'cinco', 'seis',
                                      'siete', 'ocho', 'nueve']:
                to_remove.add(palabra_vacia)
        self.__procesador_ln.Defaults.stop_words -= to_remove

    def __filtrar_palabras_vacias(self, oraciones_etiquetadas):
        # formato requerido para colocaciones (Phrases) y Word2Vec:
        # [['una', 'lista', 'de', 'oraciones']]
        oraciones_filtradas = []
        for oracion in oraciones_etiquetadas:
            oracion_filtrada = []
            for token in oracion:
                if not (token.is_stop or token.is_punct or token.is_space or
                        token.pos_ in ['AUX', 'CCONJ']):
                    oracion_filtrada.append(token.lemma_)
            oraciones_filtradas.append(oracion_filtrada)
        return oraciones_filtradas

    def __limpiar_texto(self, texto):
        char_anterior = ''
        repeticiones = 0
        indice_original = None
        i = 0
        while(i < len(texto)):
            if texto[i] == char_anterior and not texto[i].isalnum():
                repeticiones += 1
                if indice_original is None:
                    indice_original = i
            elif repeticiones >= 3:
                texto = texto[:indice_original + 2] + texto[i:]
                i = indice_original + 2
                repeticiones = 0
                indice_original = None
            char_anterior = texto[i]
            i += 1
        if repeticiones >= 3:
            texto = texto[:indice_original + 2] + texto[i - 1:]
        return texto

    def __es_descripcion_valida(self, descripcion):
        if MineroRequerimientos.patron_interrogativo.findall(descripcion):
            return False
        lon_urs = 0
        for urls in MineroRequerimientos.patron_url.findall(descripcion):
            lon_urs += len(urls)
        if lon_urs >= len(descripcion) * 0.5:
            return False
        return not self.__es_codigo(descripcion)

    def __es_codigo(self, texto):
        texto_sn_espacios = texto.replace(' ', '')
        lon_xml = 0
        for xml in BeautifulSoup(texto_sn_espacios, 'html.parser').find_all():
            lon_xml += len(str(xml))
        if lon_xml >= len(texto_sn_espacios) * 0.5:
            return True
        lon_codigo = 0
        for codigo in MineroRequerimientos.patron_codigo.findall(texto):
            lon_codigo += len(codigo)
        if lon_codigo >= len(texto) * 0.5:
            return True
        return False

    def __es_requerimiento_unico(self, requerimiento, requerimientos):
        if requerimiento in requerimientos:
            return False
        for req in requerimientos:
            titulo_menor = req.titulo\
                if len(req.titulo) <= len(requerimiento.titulo) else\
                requerimiento.titulo
            titulo_mayor = requerimiento.titulo if titulo_menor is req.titulo\
                else req.titulo
            cambios_titulo = [d for d in difflib.ndiff(
                titulo_menor, titulo_mayor) if d[0] == '-']
            if len(set(cambios_titulo)) >= 3:
                continue
            descripcion_menor = req.descripcion\
                if len(req.descripcion) <= len(requerimiento.descripcion) else\
                requerimiento.descripcion
            descripcion_mayor = requerimiento.descripcion\
                if descripcion_menor is req.descripcion else req.descripcion
            cambios_desc = [d for d in difflib.ndiff(
                descripcion_menor, descripcion_mayor) if d[0] == '-']
            if len(set(cambios_desc)) >= 3:
                continue
            if titulo_menor is req.titulo:
                req.titulo = requerimiento.titulo
            if descripcion_menor is req.descripcion:
                req.descripcion = requerimiento.descripcion
            return False
        return True

    def __registra_informacion_extraida(self, requerimientos, glosario):
        for req in requerimientos:
            logger_extraccion_req.debug(req)
        logger_extraccion_terms.debug(glosario)

    def obtener_sustantivos_clave():
        return ['sistema', 'aplicación', 'software', 'usuario', 'participante',
                'paquete', 'mensaje', 'función', 'módulo', 'interfaz',
                'interacción', 'accesibilidad', 'dato', 'información', 'botón',
                'menú', 'componente', 'rutina', 'método', 'memoria', 'tarea',
                'pantalla', 'estructura', 'conjunto', 'arreglo', 'lista',
                'alta', 'baja', 'disco', 'almacenamiento', 'dispositivo',
                'dimensión', 'parámetro', 'atributo', 'entrada', 'salida',
                'requerimiento', 'formato', 'plataforma']

    def obtener_verbos_clave():
        return ['deber', 'contar', 'poder', 'aplicar', 'hacer', 'mostrar',
                'imprimir', 'desplegar', 'regresar', 'recibir', 'devolver',
                'salir', 'incluir', 'insertar', 'agregar', 'eliminar',
                'borrar', 'suprimir', 'quitar', 'producir', 'consumir',
                'proveer', 'modificar', 'alterar', 'cambiar', 'limitar',
                'reportar', 'ejecutar', 'manipular', 'registrar', 'cambiar',
                'almacenar', 'parametrizar']

    def obtener_verbos_perifrasis():
        return ['ir', 'tener', 'haber', 'estar']

    def __extraer_palabras_clave_preprocesadas(destino, palabras_clave_etiq,
                                               map=False):
        for oracion in palabras_clave_etiq:
            for token in oracion:
                if map:
                    destino[token.lemma_] = token.text
                else:
                    destino.append(token.lemma_)
