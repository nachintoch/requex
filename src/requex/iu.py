#!/usr/bin/env python3

# This file is part of Requex.

# Requex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Requex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Requex.  If not, see <https://www.gnu.org/licenses/>.

import difflib
from customui import ToolTip
import math
import logging
from threading import Thread
from pathlib import Path
import os

import tkinter as tk
from tkinter import filedialog
from tkinter import font
from tkinter import messagebox
from tkinter import ttk
from tkinter.scrolledtext import ScrolledText

import requex
from requex.ayuda import ContenidoAyuda
from requex.modelos import Requerimiento

logger = logging.getLogger(__name__)


class ConfigIU:

    ancho_ventana = 120
    estilo = None

    def construye_campos_texto(ventana, campos_texto, textos_ayuda, row):
        iu_campos_texto = []
        for i, etiqueta_valores_omision in enumerate(campos_texto.items()):
            etiqueta, valor_omision = etiqueta_valores_omision
            tooltip = textos_ayuda[etiqueta] if etiqueta in textos_ayuda\
                else None
            iu = ConfigIU.__prepara_campo_texto(
                ventana, etiqueta, valor_omision, tooltip, row + i)
            iu_campos_texto.append(iu)
        return iu_campos_texto, row + i + 1

    def construye_selectores(ventana, selectores, textos_ayuda, row):
        iu_selectores = []
        for i, etiqueta_valores_selector in enumerate(selectores.items()):
            etiqueta, valores_selector = etiqueta_valores_selector
            tooltip = textos_ayuda[etiqueta] if etiqueta in textos_ayuda\
                else None
            valores, valor_omision = valores_selector
            iu = ConfigIU.__prepara_selector(
                ventana, etiqueta, valores, valor_omision, tooltip, row + i)
            iu_selectores.append(iu)
        return iu_selectores, row + i + 1

    def construye_boton(ventana, texto, comando, row, column=None,
                        padx=None, sticky=None, estado=None):
        boton = tk.Button(ventana, text=texto, command=comando)
        boton.grid(row=row, column=column, padx=padx, sticky=sticky)
        if estado:
            boton['state'] = estado
        return boton

    def recorta_cadena(cadena, longitud_max=ancho_ventana):
        return (cadena[:longitud_max - 3] + '...')\
            if len(cadena) > longitud_max else cadena

    def verifica_atributos_requerimiento(id, titulo, descripcion,
                                         estado_swebok, estado_essence,
                                         prioridad, volatilidad, tipo, riesgo,
                                         clausula_org):
        estado = ConfigIU.prepara_estado_requerimiento(
            estado_swebok, estado_essence)
        clausulas_validas =\
            Requerimiento.obtener_clausulas_organizacion_validas()[1]
        if type(clausula_org) is tuple:
            clausula, desc_clausula = clausula_org
            if desc_clausula == ContenidoAyuda.HINT_CARACTERES_ESPECIALES:
                desc_clausula = ''
            if clausula not in clausulas_validas:
                clausula = tipo
            clausula_org = (clausula, desc_clausula)
        elif clausula_org not in clausulas_validas:
            clausula_org = tipo
        try:
            requerimiento = Requerimiento(id, titulo, descripcion, estado)
            requerimiento.prioridad = int(prioridad) if prioridad else ''
            requerimiento.volatilidad = int(volatilidad) if volatilidad else ''
            requerimiento.tipo = tipo
            requerimiento.riesgo = riesgo
            requerimiento.clausula_organizacion = clausula_org
            return requerimiento
        except AttributeError as e:
            messagebox.showwarning('Volver', e)
            return None

    def prepara_estado_requerimiento(estado_swebok, estado_essence):
        if estado_essence == 'Ninguno':
            estado_essence = None
        if not estado_swebok and estado_essence:
            estado = estado_essence
        elif not estado_essence and estado_swebok:
            estado = estado_swebok
        else:
            estado = (estado_swebok, estado_essence)
        return estado

    def prepara_selector_archivos(ventana):
        try:
            try:
                ventana.tk.call('tk_getOpenFile', '-foobarbaz')
            except tk.TclError:
                pass
            ventana.tk.call('set', '::tk::dialog::file::showHiddenVar', '0')
        except tk.TclError:
            pass

    def confirma_cerrar_aplicacion():
        respuesta = messagebox.askyesno('¿Salir?', 'Los cambios se perderán')
        return respuesta

    def campo_texto_hint(ventana, texto_inicial=None):
        campo_texto = CampoTextoHint(ventana)
        if texto_inicial is not None:
            campo_texto.insert(0, texto_inicial)
            return campo_texto

        campo_texto.hint = ContenidoAyuda.HINT_CARACTERES_ESPECIALES
        return campo_texto

    def crea_etiqueta(ventana, contenido_etiqueta, texto_tooltip=None,
                      es_titulo=False, **grid_kwargs):
        kwargs = {}
        if type(contenido_etiqueta) is tk.PhotoImage:
            kwargs['image'] = contenido_etiqueta
        elif type(contenido_etiqueta) is tk.StringVar:
            kwargs['textvariable'] = contenido_etiqueta
        else:
            kwargs['text'] = contenido_etiqueta
        if es_titulo:
            kwargs['font'] = 'Helvetica 12 bold'
        etiqueta = tk.Label(ventana, wraplength=1000, **kwargs)
        etiqueta.grid(**grid_kwargs)
        if texto_tooltip:
            ToolTip(etiqueta, texto_tooltip)
        return etiqueta

    def carga_imagen(ventana, nombre):
        return tk.PhotoImage(file=os.sep.join(['res', 'img', nombre]),
                             master=ventana)

    def crea_ventana(titulo, raiz=None):
        ventana = ToplevelCentrado(raiz) if raiz else VentanaCentrada()
        ventana.resizable(width=False, height=False)
        ventana.title(titulo)
        ventana.tk.call('wm', 'iconphoto', ventana._w,
                        ConfigIU.carga_imagen(ventana, 'icono_requex-48.png'))
        return ventana

    def crea_lista_seleccionable(ventana, valores, nombres_columnas,
                                 evento_seleccion, no_renglones):
        ConfigIU.__verifica_estilo(ventana)
        lista = ttk.Treeview(ventana, columns=nombres_columnas,
                             selectmode=tk.BROWSE, height=no_renglones)
        valores.sort()

        ancho_min, ancho_columnas, relacion_indice_lista_id =\
            ConfigIU.__llena_lista_seleccionable(
                lista, valores, nombres_columnas)
        ConfigIU.__configura_lista_seleccionable(
            ventana, lista, nombres_columnas, ancho_min, ancho_columnas,
            evento_seleccion)
        return lista, relacion_indice_lista_id

    def actualiza_lista_seleccionable(lista, indice_lista,
                                      relacion_indice_lista_id, elemento=None):
        len_lista = len(lista.get_children())
        if elemento:  # alta o cambios
            id, valor, estado = elemento
            color_fondo = 'green' if estado else 'orange'
            if indice_lista < len_lista:
                # las altas deben ser al final de la lista
                # esto se interpreta como un cambio (sobreescribe el valor)
                lista.delete([indice_lista])
            relacion_indice_lista_id[indice_lista] = id
            lista.insert('', indice_lista, indice_lista, values=valor,
                         tags=[color_fondo])
        else:  # baja
            # recorre todos los elementos a la posición anterior para conservar
            # la identificación basada en los índices de la lista
            for i in range(indice_lista, len_lista - 1):
                elemento = lista.item(i + 1)
                lista.delete([i])
                lista.insert('', i, i, values=elemento['values'],
                             tags=elemento['tags'])
                relacion_indice_lista_id[i] = relacion_indice_lista_id[i + 1]
            lista.delete([len_lista - 1])
            del relacion_indice_lista_id[len_lista - 1]
        return relacion_indice_lista_id

    def formatea_requerimientos(requerimientos):
        lista_requerimientos = [(req.id, (
            req.id, req.titulo, req.prioridad, req.tipo,
            req.clausula_organizacion[0] if req.clausula_organizacion else ''),
            req.verificar_completo())
            for req in requerimientos.values()]
        etiquetas_columnas = ['ID', 'Requerimiento', 'Prioridad', 'Tipo',
                              'Cláusula org']
        return etiquetas_columnas, lista_requerimientos

    def formatea_glosario(glosario):
        lista_entradas = [
            (entrada.id, (', '.join(entrada.terminos), entrada.definicion),
             entrada.verificar_completo()) for entrada in glosario.values()]
        etiquetas_columnas = ['Términos', 'Definición']
        return etiquetas_columnas, lista_entradas

    def formatea_responsables_emision(responsables):
        # TODO con el issue #27 se puede aprovechar un modelo de los
        # responsables; que podría indicar si el responsable está completo
        # (similar al caso de Requerimiento)
        lista_responsables = [
            (id, (id, responsable), True) for id, responsable in
            responsables.items()]
        etiquetas_columnas = ['ID', 'Nombre de responsable']
        return etiquetas_columnas, lista_responsables

    def formatea_referencias(referencias):
        # TODO con el issue #32 se puede aprovechar un modelo de las
        # referencias que ayude a determinar si cada instancia es completa
        lista_referencias = [
            (id, (id, referencia), True) for id, referencia in
            referencias.items()]
        etiquetas_columnas = ['ID', 'Referencia']
        return etiquetas_columnas, lista_referencias

    def construye_ventana_inicial(dir_corpus, nombre_proyecto):
        pantalla_inicial = ConfigIU.crea_ventana(
            'REQUEX - Preparación del análisis')
        ConfigIU.crea_etiqueta(
            pantalla_inicial, 'Directorio con los documentos a analizar:',
            ContenidoAyuda.obtener_ayuda_directorio_analisis(), row=0, column=0)
        corpus = None
        if dir_corpus:
            corpus = dir_corpus
        dir_corpus = tk.StringVar(pantalla_inicial)
        dir_corpus.set(corpus if corpus else str(Path.home()))
        ConfigIU.crea_etiqueta(pantalla_inicial, dir_corpus, row=0, column=1)

        def elegir_corpus():
            dir = filedialog.askdirectory(
                initialdir=dir_corpus.get(),
                title='Elija el directorio con los archivos de entrada')
            if dir:
                dir_corpus.set(dir)
        boton = tk.Button(pantalla_inicial, text='Elegir directorio',
                          command=elegir_corpus)
        boton.grid(row=0, column=2)

        ConfigIU.prepara_selector_archivos(pantalla_inicial)
        ConfigIU.crea_etiqueta(
            pantalla_inicial, 'Nombre del proyecto:',
            ContenidoAyuda.obtener_ayuda_nombre_proyecto(), row=1, column=0)
        nombre = None
        if nombre_proyecto:
            nombre = nombre_proyecto
        nombre_proyecto = ConfigIU.campo_texto_hint(pantalla_inicial)
        nombre_proyecto.grid(row=1, column=1, columnspan=2)
        if nombre:
            nombre_proyecto.insert(tk.INSERT, nombre)

        boton = tk.Button(
            pantalla_inicial, text='Extraer requerimientos',
            command=lambda: requex.iniciar_extraccion(
                dir_corpus.get(), nombre_proyecto, pantalla_inicial))
        boton.grid(row=2, column=2)

        separador = ttk.Separator(pantalla_inicial, orient='horizontal')
        separador.grid(row=3, column=0, columnspan=3, sticky='EW')

        ConfigIU.crea_etiqueta(
            pantalla_inicial, ContenidoAyuda.obtener_nombre_descripcion(),
            es_titulo=True, row=4, column=0, columnspan=3)
        logo = ConfigIU.carga_imagen(pantalla_inicial, 'icono_requex-256.png')
        pantalla_inicial.logo = logo
        ConfigIU.crea_etiqueta(pantalla_inicial, logo, row=5, column=0,
                               columnspan=3)
        ConfigIU.crea_etiqueta(
            pantalla_inicial, ContenidoAyuda.obtener_descripcion_general(),
            row=6, column=0, columnspan=3)
        return pantalla_inicial

    def muestra_url_repo_licencia(raiz, cerrar):
        iu_licencia = ConfigIU.crea_ventana('REQUEX - Proyecto y licencia',
                                            raiz)
        iu_licencia.protocol("WM_DELETE_WINDOW", cerrar)
        ConfigIU.crea_etiqueta(iu_licencia, 'URL del proecto; repositorio GIT:',
                               es_titulo=True, row=0, column=0)
        ConfigIU.crea_etiqueta(iu_licencia,
                               'https://gitlab.com/nachintoch/requex',
                               row=1, column=0)
        ConfigIU.crea_etiqueta(iu_licencia, 'Licencia GNU GPLv3',
                               es_titulo=True, row=2, column=0)
        widget_licencia = ScrolledText(iu_licencia,
                                       width=ConfigIU.ancho_ventana, height=25)
        widget_licencia.grid(row=3, column=0)
        with open(os.sep.join(['res', 'gpl-3.0.txt'])) as lic:
            widget_licencia.insert(tk.INSERT, lic.read())
        ConfigIU.construye_boton(iu_licencia, 'Volver', cerrar, row=4)
        iu_licencia.mainloop()

    def fixed_map(option):
        """
        Resuelve un bug en tkinter 8.6.9 que ignora el cambio de color de fondo
        en Treeview
        https://bugs.python.org/msg342678
        """
        return [elm for elm in
                ConfigIU.estilo.map('Treeview', query_opt=option) if
                elm[:2] != ('!disabled', '!selected')]

    def __prepara_campo_texto(ventana, texto_etiqueta, por_omision,
                              texto_tooltip, row, column=None):
        if texto_etiqueta is not None:
            if not texto_etiqueta:
                texto_etiqueta = '\t'
            ConfigIU.crea_etiqueta(ventana, texto_etiqueta, texto_tooltip,
                                   row=row, column=column)
        iu_campo_texto = ConfigIU.campo_texto_hint(ventana, por_omision)
        iu_campo_texto.grid(row=row, column=column + 1 if column else 1)
        return iu_campo_texto

    def __prepara_selector(ventana, texto_etiqueta, rango_lista, por_omision,
                           texto_tooltip, row, column=None):
        if texto_etiqueta is not None:
            if not texto_etiqueta:
                texto_etiqueta = '\t'
            ConfigIU.crea_etiqueta(ventana, texto_etiqueta, texto_tooltip,
                                   row=row, column=column)
            if column:
                column += 1
            else:
                column = 1

        var_seleccion = tk.StringVar(ventana)
        if type(rango_lista) is range:
            rango_lista = [i for i in rango_lista]

        if por_omision is not None:
            var_seleccion.set(por_omision)
        iu_selector = tk.OptionMenu(ventana, var_seleccion, *rango_lista)
        iu_selector.grid(row=row, column=column)
        return iu_selector, var_seleccion

    def __verifica_estilo(ventana):
        if not ConfigIU.estilo:
            raiz = ventana.nametowidget(ventana.winfo_parent())
            ConfigIU.estilo = ttk.Style(raiz)
            ConfigIU.estilo.configure('Treeview', font=('Helvetica', 11),
                                      rowheight=30)
            # Quitar si tkinter 8.6.9 es obsoleto
            # tkinter bug --> https://core.tcl-lang.org/tk/info/509cafafae
            ConfigIU.estilo.map('Treeview',
                                foreground=ConfigIU.fixed_map('foreground'),
                                background=ConfigIU.fixed_map('background'))

    def __calcula_ancho_minimo(nombres_columnas, ancho_inicial, tipografia):
        ancho_min = {}
        for nombre in nombres_columnas:
            ancho = math.ceil(tipografia.measure(nombre) * 1.75)
            ancho_min[nombre] = ancho_inicial\
                if ancho < ancho_inicial else ancho
        return ancho_min

    def __llena_lista_seleccionable(lista, valores, nombres_columnas):
        tipografia = font.Font(family='Helvetica', size=11, weight='normal')
        no_columnas = len(nombres_columnas)
        ancho_inicial = tipografia.measure('l' * ConfigIU.ancho_ventana)
        ancho_inicial = math.floor(ancho_inicial / no_columnas)
        ancho_min = ConfigIU.__calcula_ancho_minimo(nombres_columnas,
                                                    ancho_inicial, tipografia)
        relacion_indice_lista_id = {}
        ancho_columnas = ConfigIU.__inicializa_ancho_columnas(nombres_columnas)
        i = 0
        for id, valor, completo in valores:
            valor = list(valor)
            ancho_columnas = ConfigIU.__calcula_ancho_columnas(
                ancho_columnas, nombres_columnas, ancho_inicial, valor,
                tipografia)
            relacion_indice_lista_id[i] = id
            color_fondo = 'green' if completo else 'orange'
            lista.insert('', i, i, values=valor, tags=[color_fondo])
            i += 1

        lon_excedente = 0
        columnas_excedentes = []
        for columna, ancho in ancho_columnas.items():
            excedente = ancho_inicial - ancho
            if excedente > 0:
                lon_excedente += excedente
            else:
                columnas_excedentes.append(columna)
        no_columnas_excedentes = len(columnas_excedentes)
        lon_excedente = math.ceil(lon_excedente / no_columnas_excedentes)\
            if no_columnas_excedentes else 0
        for columna in columnas_excedentes:
            ancho_columnas[columna] += lon_excedente
        return ancho_min, ancho_columnas, relacion_indice_lista_id

    def __configura_lista_seleccionable(
            ventana, lista, nombres_columnas, ancho_min, ancho_columnas,
            evento_seleccion):
        for columna in nombres_columnas:
            lista.heading(columna, text=columna)
            lista.column(columna, anchor='center', minwidth=ancho_min[columna],
                         width=ancho_columnas[columna])
        lista.tag_configure('green', background='green')
        lista.tag_configure('orange', background='orange')

        lista.bind('<<TreeviewSelect>>', evento_seleccion)
        lista.grid(row=1, column=0, columnspan=3, sticky='WE')

        scroll = tk.Scrollbar(ventana, orient=tk.VERTICAL)
        scroll.grid(row=1, column=3, sticky='NSW')
        lista.configure(yscrollcommand=scroll.set)
        scroll.config(command=lista.yview)
        lista.column('#0', width=0)

    def __formatea_atributo(atributo):
        if atributo is None:
            atributo = ''
        return ConfigIU.recorta_cadena(str(atributo))

    def __inicializa_ancho_columnas(nombres_columnas):
        ancho_columnas = {}
        for columna in nombres_columnas:
            ancho_columnas[columna] = 0
        return ancho_columnas

    def __calcula_ancho_columnas(ancho_columnas, nombres_columnas,
                                 ancho_inicial, valor_actual, tipografia):
        for j, columna in enumerate(nombres_columnas):
            valor_actual[j] = ConfigIU.__formatea_atributo(valor_actual[j])
            ancho = ancho_columnas[columna]
            if ancho == ancho_inicial:
                continue
            ancho_actual = tipografia.measure(valor_actual[j]) + 10
            if ancho_actual > ancho:
                ancho_columnas[columna] = ancho_actual
        return ancho_columnas


class CampoTextoHint(tk.Entry):

    def __init__(self, ventana):
        super().__init__(ventana, width=50)
        self.__ventana = ventana
        self.__hint = None

    @property
    def hint(self):
        return self.__hint

    @hint.setter
    def hint(self, hint):
        if self.hint is not None:
            raise AttributeError('El hint únicamente puede asignarse una vez')
        if self.get():
            raise AttributeError('El hint únicamente puede asignarse cuando el '
                                 + 'campo de texto es vacío')
        contenido = tk.StringVar(self.__ventana)
        contenido.set(hint)

        def borra_hint(nombre_widget, indice, modo):
            valor = contenido.get()
            diff = [d[-1] for d in difflib.ndiff(hint, valor) if d[0] == '+']
            diff = ''.join(diff)
            self.delete(0, tk.END)
            self.config(fg='black')
            contenido.trace_remove('write', nombre_borra_hint)
            self.insert(0, diff)
        nombre_borra_hint = contenido.trace_add('write', borra_hint)
        self.config(fg='gray', textvariable=contenido)
        self.__hint = hint

    def get(self):
        valor = super().get()
        return '' if valor == self.hint else valor


class VentanaCentrada(tk.Tk):

    def __init__(self):
        super().__init__()

    def mainloop(self):
        super().update()
        media_dimension = (self.winfo_width() / 2, self.winfo_height() / 2)
        media_pantalla = (self.winfo_screenwidth() / 2,
                          self.winfo_screenheight() / 2)
        posicion_x = int(media_pantalla[0] - media_dimension[0])
        posicion_y = int(media_pantalla[1] - media_dimension[1])
        self.geometry('+{}+{}'.format(posicion_x, posicion_y))
        super().mainloop()


class ToplevelCentrado(tk.Toplevel):

    def __init__(self, raiz):
        super().__init__(raiz)
        self.__raiz = raiz

    def mainloop(self):
        super().update()
        media_dimension = (self.winfo_width() / 2, self.winfo_height() / 2)
        media_pantalla = (self.__raiz.winfo_screenwidth() / 2,
                          self.__raiz.winfo_screenheight() / 2)
        posicion_x = int(media_pantalla[0] - media_dimension[0])
        posicion_y = int(media_pantalla[1] - media_dimension[1])
        self.geometry('+{}+{}'.format(posicion_x, posicion_y))
        super().mainloop()


class HiloCarga(Thread):

    def __init__(self, nombre_proyecto, dir_corpus):
        texto_mensaje = ContenidoAyuda.obtener_mensaje_carga(nombre_proyecto,
                                                             dir_corpus)
        super().__init__(target=self.__muentra_mensaje_carga,
                         args=(texto_mensaje,))

    def __muentra_mensaje_carga(self, texto_mensaje):
        self.__mensaje_cargando = ConfigIU.crea_ventana(
            'Extrayendo requerimientos...')
        ConfigIU.crea_etiqueta(self.__mensaje_cargando, texto_mensaje)
        self.__mensaje_cargando.protocol(
            "WM_DELETE_WINDOW", lambda: None)
        self.__mensaje_cargando.mainloop()

    def terminar(self):
        self.__mensaje_cargando.withdraw()
        self.__mensaje_cargando.quit()
