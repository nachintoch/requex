# This file is part of Requex.

# Requex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Requex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Requex.  If not, see <https://www.gnu.org/licenses/>.

import copy
from datetime import datetime
import logging
import re
import string

logger = logging.getLogger(__name__)


class EspecificacionRequerimientosSoftware:

    def __init__(self):
        self.__titulo = ''
        self.__alcances = None
        self.__version = None
        self.__estado = 0
        self.__responsables_emision = {}
        self.__tabla_contenidos = None
        self.__referencias = {}
        self.__glosario = Glosario()
        self.__requerimientos = {}
        self.__finalizado = False

    # métodos de acceso y modificación

    @property
    def titulo(self):
        return self.__titulo

    @titulo.setter
    def titulo(self, titulo):
        if not titulo:
            raise AttributeError('El titulo del documento no puede ser vacio')
        if type(titulo) is not str:
            raise AttributeError('El titulo del documento debe ser una cadena')
        titulo = titulo.strip()
        if len(titulo) < 5:
            raise AttributeError('El titulo del documento debe tener 5 '
                                 + 'caracteres')
        self.__titulo = titulo

    @property
    def estado(self):
        return self.__estado

    @property
    def fecha_emision(self):
        return self.__fecha_emision if\
            hasattr(self,
                    '_EspecificacionRequerimientosSoftware__fecha_emision')\
            else None

    @property
    def version(self):
        return self.__version

    @version.setter
    def version(self, version):
        self.__verificar_finalizado()
        if not version:
            raise AttributeError('La versión del docmento no puede ser vacía')
        if type(version) is not str:
            raise AttributeError('La versión del documento debe ser una '
                                 + 'cadena')
        self.__version = version

    @property
    def responsables_emision(self):
        return self.__responsables_emision

    @property
    def tabla_contenidos(self):
        return self.__tabla_contenidos

    @property
    def alcances(self):
        return self.__alcances

    @alcances.setter
    def alcances(self, alcances):
        self.__verificar_finalizado()
        if not alcances:
            raise AttributeError('Los alcances del documento no pueden ser '
                                 + 'vacios')
        if type(alcances) is not str:
            raise AttributeError('Los alcances del documento deben ser una '
                                 + 'cadena')
        alcances = alcances.strip()
        if len(alcances) < 20:
            raise AttributeError('Los alcances del documento deben tener al '
                                 + 'menos 20 caracteres')
        self.__alcances = alcances

    @property
    def glosario(self):
        return self.__glosario

    @property
    def requerimientos(self):
        return self.__requerimientos

    @property
    def referencias(self):
        return self.__referencias

    # métodos de implementación

    def agregar_requerimiento(self, requerimiento):
        if not requerimiento or type(requerimiento) is not Requerimiento:
            raise AttributeError('El requerimiento debe ser una instancia de '
                                 + 'requex.modelos.Requerimiento')
        self.requerimientos[requerimiento.id] = requerimiento
        return requerimiento.id

    def suprimir_requerimiento(self, no_requerimiento):
        self.__verificar_finalizado()
        if no_requerimiento not in self.requerimientos:
            return False
        del self.requerimientos[no_requerimiento]
        return True

    def agregar_referencia(self, referencia):
        self.__verificar_finalizado()
        if not referencia:
            raise AttributeError('La referencia no puede ser vacia')
        if type(referencia) is not str:
            raise AttributeError('La referencia debe ser una cadena')
        id = len(self.__referencias)
        self.__referencias[id] = referencia
        return id

    def suprimir_referencia(self, no_referencia):
        self.__verificar_finalizado()
        if no_referencia not in self.__referencias:
            return False
        del self.__referencias[no_referencia]
        return True

    def agregar_responsable_emision(self, responsable):
        self.__verificar_finalizado()
        if not responsable:  # sin postre por una semana
            raise AttributeError('El responsable de emisión no puede ser '
                                 + 'vacío')
        if type(responsable) is not str:
            raise AttributeError('El responsable de emisión debe ser una '
                                 + 'cadena')
        id = len(self.__responsables_emision)
        self.__responsables_emision[id] = responsable
        return id

    def suprimir_responsable_emision(self, no_responsable):
        self.__verificar_finalizado()
        if no_responsable not in self.__responsables_emision:
            return False
        del self.__responsables_emision[no_responsable]
        return True

    def verificar_completo(self):
        if not self.titulo or not self.alcances or not self.version or \
                not self.__referencias or\
                (not self.__responsables_emision and not self.__glosario and
                 not self.__requerimientos):
            self.__estado = 0
        elif self.__verifica_revision_incompleta():
            self.__estado = 1
        else:
            self.__estado = 2
        return self.__estado

    def __verifica_revision_incompleta(self):
        if (not self.__responsables_emision or not self.__glosario or
            not self.__requerimientos) or\
                (not self.__glosario.verificar_completo()):
            return True
        for referencia in self.__referencias.values():
            if not referencia:
                return True
        for requerimiento in self.__requerimientos.values():
            if not requerimiento.verificar_completo():
                return True
        return False

    def finalizar(self):
        self.__verificar_finalizado()
        self.__fecha_emision = datetime.now()
        self.__tabla_contenidos = {}
        for requerimiento in self.__requerimientos.values():
            if requerimiento.clausula_organizacion\
                    not in self.__tabla_contenidos:
                self.__tabla_contenidos[
                    requerimiento.clausula_organizacion] = []
            self.__tabla_contenidos[requerimiento.clausula_organizacion].append(
                requerimiento.id)
        self.__finalizado = True

    def __verificar_finalizado(self):
        if self.__finalizado:
            raise Exception('El documento ya ha sido finalizado')
        pass

    def __str__(self):
        return 'EspecificacionRequerimientosSoftware(titulo="' + self.titulo\
            + ',estado=' + str(self.estado) + ',fecha_emision='\
            + str(self.fecha_emision) + ',version=' + self.version\
            + ',responsables_emision=' + str(self.responsables_emision)\
            + ',tabla_contenidos=' + str(self.tabla_contenidos) + ',alcances='\
            + self.alcances + ',glosario=' + str(self.glosario)\
            + ',requerimientos=' + str(self.requerimientos) + ',referencias='\
            + str(self.referencias) + ',finalizado=' + str(self.__finalizado)\
            + '")'

    def __eq__(self, otro):
        if not otro or type(otro) is not EspecificacionRequerimientosSoftware:
            return False
        return self.titulo == otro.titulo and self.estado == otro.estado and\
            self.fecha_emision == otro.fecha_emision and\
            self.version == otro.version and\
            self.responsables_emision == otro.responsables_emision and\
            self.tabla_contenidos == otro.tabla_contenidos and\
            self.glosario == otro.glosario and\
            self.requerimientos == otro.requerimientos and\
            self.referencias == otro.referencias

    def __hash__(self):
        return hash((self.titulo, self.estado, self.fecha_emision,
                    self.version, self.glosario, self.tabla_contenidos,
                    frozenset(self.responsables_emision.values()),
                    frozenset(self.requerimientos.values()),
                    self.alcances, frozenset(self.referencias.values())))

    def obtener_estados_validos():
        return ['No revisado', 'Revisión incompleta', 'Revisado']


class Requerimiento:

    clase_caracteres_vacios = '[' + string.whitespace\
        + r'\.,;:¿\?\'"¡!·\(\)\{\}\-_\[\]]'

    def __init__(self, id, titulo, descripcion, estado):
        if id is None:
            raise AttributeError('El identificador de un requerimiento no '
                                 + 'puede ser vacio')
        if type(id) is not int:
            raise AttributeError('El identificador de un requerimiento debe '
                                 + 'ser un entero')
        self.__id = id
        self.titulo = titulo
        self.descripcion = descripcion
        self.estado = estado
        self.clausula_organizacion = None

    # métodos de acceso y modificación

    @property
    def id(self):
        return self.__id

    @property
    def titulo(self):
        return self.__titulo

    @titulo.setter
    def titulo(self, titulo):
        if not titulo:
            raise AttributeError('El titulo de un requerimiento no puede '
                                 + 'ser vacío')
        if type(titulo) is not str:
            raise AttributeError('El titulo de un requerimiento debe ser '
                                 + 'una cadena')
        titulo = titulo.strip()
        if len(re.sub(Requerimiento.clase_caracteres_vacios, '', titulo)) < 3:
            raise AttributeError('El titulo de un requerimiento debe tener'
                                 + ' cuando menos tres caracteres no vacios')
        self.__titulo = titulo

    @property
    def prioridad(self):
        return self.__prioridad\
            if hasattr(self, '_Requerimiento__prioridad') else None

    @prioridad.setter
    def prioridad(self, prioridad):
        if type(prioridad) is not int:
            raise AttributeError('La prioridad de un requerimiento debe '
                                 + 'ser un entero')
        if prioridad < 0 or prioridad > 10:
            raise AttributeError('La prioridad de un requerimiento debe '
                                 + 'tener un valor en el intervalo [0,10]')
        self.__prioridad = prioridad

    @prioridad.deleter
    def prioridad(self):
        del self.__prioridad

    @property
    def descripcion(self):
        return self.__descripcion

    @descripcion.setter
    def descripcion(self, descripcion):
        if not descripcion:
            raise AttributeError('La descripción de un requerimiento no '
                                 + 'puede ser vacia')
        if type(descripcion) is not str:
            raise AttributeError('La descripción de un requerimiento debe'
                                 + ' ser una cadena')
        descripcion = descripcion.strip()
        self.__valida_descripcion(descripcion)
        self.__descripcion = descripcion

    @property
    def estado(self):
        return self.__estado

    @estado.setter
    def estado(self, estado):
        if not estado:
            raise AttributeError('El estado de un requerimiento no puede '
                                 + 'ser vacio')
        if type(estado) is not tuple:
            self.__estado = (estado, None) if self.__validar_estado(estado)\
                else (None, estado)
            return
        elif len(estado) != 2:
            raise AttributeError('El estado de un requerimiento debe '
                                 + 'ser una tupla (str,str) o una cadena')
        for i in range(len(estado)):
            self.__validar_estado(estado[i])
        self.__estado = estado

    @property
    def volatilidad(self):
        return self.__volatilidad if \
            hasattr(self, '_Requerimiento__volatilidad') else None

    @volatilidad.setter
    def volatilidad(self, volatilidad):
        if type(volatilidad) is not int:
            raise AttributeError('La volatilidad de un requerimiento '
                                 + 'debe ser un entero')
        if volatilidad < 0 or volatilidad > 10:
            raise AttributeError('La volatilidad de un requerimiento debe'
                                 + ' tener un valor en el intervalo [0,10]')
        self.__volatilidad = volatilidad

    @volatilidad.deleter
    def volatilidad(self):
        del self.__volatilidad

    @property
    def tipo(self):
        return self.__tipo if hasattr(self, '_Requerimiento__tipo') else None

    @tipo.setter
    def tipo(self, tipo):
        if type(tipo) is not str:
            raise AttributeError('El tipo de un requerimiento debe ser '
                                 + 'una cadena')
        tipos_validos = Requerimiento.obtener_tipos_validos()
        if tipo not in tipos_validos:
            raise AttributeError('El tipo de un requerimiento debe ser alguno '
                                 + 'de los siguientes: ' + str(tipos_validos))
        self.__tipo = tipo

    @tipo.deleter
    def tipo(self):
        del self.__tipo

    @property
    def riesgo(self):
        return self.__riesgo if hasattr(self, '_Requerimiento__riesgo') else ''

    @riesgo.setter
    def riesgo(self, riesgo):
        if type(riesgo) is not str:
            raise AttributeError('El riesgo de un requerimiento debe ser '
                                 + 'una cadena')
        self.__riesgo = riesgo

    @riesgo.deleter
    def riesgo(self):
        del self.__riesgo

    @property
    def clausula_organizacion(self):
        return self.__clausula_organizacion

    @clausula_organizacion.setter
    def clausula_organizacion(self, clausula):
        if clausula is None:
            self.__clausula_organizacion = clausula
            return
        if type(clausula) is not tuple:
            if type(clausula) is not str:
                raise AttributeError('La cláusula de organización de un '
                                     + 'requerimiento debe ser una tupla '
                                     + '(str,str) o una cadena')
            tipos_validos = Requerimiento.obtener_tipos_validos()
            if clausula not in tipos_validos:
                raise AttributeError('En caso de que la cláusula de '
                                     + 'organización de un requerimiento sea '
                                     + 'una cadena; debe ser alguno de los '
                                     + 'tipos de requerimiento válidos: '
                                     + str(tipos_validos))
            self.__clausula_organizacion = (clausula, None)
            return
        elif len(clausula) != 2:
            raise AttributeError('La cláusula de organización de un '
                                 + 'requerimiento debe ser una tupla '
                                 + '(str,str) o una cadena')
        organizacion_tipos, organizacion_funcion = \
            Requerimiento.obtener_clausulas_organizacion_validas()
        descripcion = None
        if clausula[0] in organizacion_funcion and\
                (not clausula[1] or len(clausula[1].strip()) < 5):
            raise AttributeError('En caso de que la cláusula de organización de'
                                 + ' un requerimiento no sea por su tipo; la '
                                 + 'descripción debe ser de al menos 5 '
                                 + 'caracteres')
        else:
            descripcion = clausula[1].strip()
        self.__clausula_organizacion = (clausula[0], descripcion)

    @clausula_organizacion.deleter
    def clausula_organizacion(self):
        del self.__clausula_organizacion

    # métodos de implementación

    def verificar_completo(self):
        return not (self.prioridad is None or self.volatilidad is None or
                    not self.tipo or not self.riesgo or
                    not self.clausula_organizacion)

    def __validar_estado(self, estado):
        if type(estado) is not str:
            raise AttributeError('El estado de un requerimiento debe ser '
                                 + 'una tupla (str,str) o una cadena')
        estados_swebok, estados_essence = \
            Requerimiento.obtener_estados_validos()
        if(estado in estados_swebok):
            return True
        if(estado in estados_essence):
            return False
        raise AttributeError('El estado de un requerimiento debe ser alguno de'
                             + ' los siguentes ' + str(estados_swebok) + '; o '
                             + ' bien ' + str(estados_essence) + ' o una '
                             + 'combinación de un valor en cada conjunto')

    def __valida_descripcion(self, descripcion):
        if len(re.sub(Requerimiento.clase_caracteres_vacios,
                      '',  descripcion)) < 7:
            raise AttributeError('La descripción de un requerimiento debe tener'
                                 + ' cuando menos 7 caracteres no vacíos')
        no_palabras = 0
        for palabra in descripcion.split():
            if re.sub(Requerimiento.clase_caracteres_vacios, '', palabra):
                no_palabras += 1
            if no_palabras >= 3:
                break
        if no_palabras < 3:
            raise AttributeError('La descripción de un requerimiento debe '
                                 + 'formarse por al menos tres palabras')

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        repr = 'Requerimiento(id=' + str(self.id) + ',titulo=' + self.titulo\
            + ',descripcion=' + self.descripcion
        if self.prioridad is not None:
            repr += ',prioridad=' + str(self.prioridad)
        if self.estado is not None:
            repr += ',estado=' + str(self.estado)
        if self.volatilidad is not None:
            repr += ',volatilidad=' + str(self.volatilidad)
        if self.tipo is not None:
            repr += ',tipo=' + str(self.tipo)
        if self.riesgo:
            repr += ',riesgo=' + self.riesgo
        if self.clausula_organizacion is not None:
            repr += ',clausula_organizacion=' + str(self.clausula_organizacion)
        repr += ')'
        return repr

    def __eq__(self, otro):
        if not otro or type(otro) is not Requerimiento:
            return False
        return self.titulo == otro.titulo and\
            self.prioridad == otro.prioridad and\
            self.descripcion == otro.descripcion and \
            self.estado == otro.estado and \
            self.volatilidad == otro.volatilidad and self.tipo == otro.tipo\
            and self.riesgo == otro.riesgo and\
            self.clausula_organizacion == otro.clausula_organizacion

    def __hash__(self):
        return hash((self.titulo, self.prioridad, self.descripcion,
                    self.estado, self.volatilidad, self.tipo, self.riesgo,
                    self.clausula_organizacion))

    def obtener_estados_validos():
        return ['En progreso', 'En revisión', 'Suspendido', 'Terminado'],\
            ['Identificado', 'Descrito', 'Implementado', 'Verificado']

    def obtener_tipos_validos():
        return ['Funcional', 'Interfaz', 'Calidad', 'No funcional',
                'Usabilidad', 'Factor humano']

    def obtener_clausulas_organizacion_validas():
        return Requerimiento.obtener_tipos_validos(),\
            ['Modo del sistema', 'Clase de usuario', 'Objeto',
             'Característica', 'Estímulo', 'Respuesta', 'Jerarquía funcional']


class Glosario:

    __generador_id_entradas = 0

    def __init__(self):
        self.__contenido = {}
        self.__terminos = set()
        self.__finalizado = False

    def agregar_entrada(self, termino, definicion=None):
        self.__verificar_ausencia(termino)
        id = int(Glosario.__generador_id_entradas)
        entrada = Glosario._Entrada(self, id, termino, definicion)
        self.__contenido[id] = entrada
        self.__terminos.update(entrada.terminos)
        Glosario.__generador_id_entradas += 1
        return id

    def suprimir_entrada(self, no_entrada):
        self.verificar_finalizado()
        if no_entrada not in self.__contenido:
            return False
        for termino in self.__contenido[no_entrada].terminos:
            self.__terminos.remove(termino)
        del self.__contenido[no_entrada]
        return True

    def agregar_sinonimo(self, no_entrada, sinonimo):
        self.__verificar_ausencia(sinonimo)
        if no_entrada not in self.__contenido:
            return False
        entrada = self.__contenido[no_entrada]
        entrada._agregar_sinonimo(sinonimo)
        self.__terminos.add(sinonimo)
        return True

    def suprimir_sinonimo(self, no_entrada, sinonimo):
        if no_entrada not in self.__contenido:
            return False
        if sinonimo not in self.__terminos:
            return False
        entrada = self.__contenido[no_entrada]
        entrada._suprimir_sinonimo(sinonimo)
        self.__terminos.remove(sinonimo)
        return True

    def editar_definicion(self, no_entrada, definicion):
        if no_entrada not in self.__contenido:
            return False
        entrada = self.__contenido[no_entrada]
        entrada.definicion = definicion
        return True

    def obtener_id_entrada(self, termino):
        if termino not in self.__terminos:
            return None
        for entrada in self.__contenido.values():
            for term in entrada.terminos:
                if term == termino:
                    return entrada.id
        raise LookupError('El término "' + termino + '" está registrado en el '
                          + 'glosario, pero no como parte de su contenido')

    def obtener_entrada(self, id):
        if id not in self.__contenido:
            raise KeyError('No existe ninguna entrada identificada con "{}"'
                           .format(id))
        return self.__contenido[id]

    def editar_entrada(self, id, termino, definicion):
        for entrada in self.__contenido.values():
            if entrada.id == id:
                continue
            if termino in entrada.terminos:
                return False
        self.suprimir_entrada(id)
        entrada = Glosario._Entrada(self, id, termino, definicion)
        self.__contenido[id] = entrada
        self.__terminos.update(entrada.terminos)
        return True

    def verificar_completo(self):
        if not self:
            return False
        for entrada in self.__contenido.values():
            if not entrada.verificar_completo():
                return False
        return True

    def finalizar(self):
        self.__finalizado = True

    def verificar_finalizado(self):
        if self.__finalizado:
            raise Exception('El glosario ya ha sido finalizado')

    def values(self):
        return self.__contenido.values()

    def __verificar_ausencia(self, termino):
        if self.__contains__(termino):
            raise AttributeError('El término: ' + termino + ' ya está '
                                 + 'registrado en el glosario')

    def __contains__(self, termino):
        if type(termino) is not str:
            raise AttributeError('No es posible verificar la presencia de más '
                                 + 'de un término a la vez')
        return termino in self.__terminos

    def __len__(self):
        return len(self.__contenido)

    def __bool__(self):
        return len(self) > 0

    def __str__(self):
        return 'Glosario(contenido=' + str(self.__contenido) + ',terminos='\
            + str(self.__terminos) + ',finalizado=' + str(self.__finalizado)\
            + ')'

    def __eq__(self, otro):
        if type(otro) is not Glosario:
            return False
        return self.__contenido == otro.__contenido

    def __hash__(self):
        return hash(frozenset(self.__contenido.values()))

    class _Entrada:

        def __init__(self, glosario, id, terminos, definicion):
            if type(glosario) is not Glosario:
                raise AttributeError('Debe proporcionar la referencia al '
                                     + 'glosario al que pertenece esta entrada')
            if type(id) is not int:
                raise AttributeError('El identificador de una entrada del '
                                     + 'glosario debe ser un entero')
            self.__glosario = glosario
            self.__id = id
            self.terminos = terminos
            self.definicion = definicion

        @property
        def id(self):
            return self.__id

        @property
        def terminos(self):
            return self.__terminos

        @terminos.setter
        def terminos(self, terminos):
            self.__glosario.verificar_finalizado()
            terminos = Glosario._Entrada.__formatea_termino(terminos)
            if not terminos:
                raise AttributeError('Los términos en el glosario deben ser una'
                                     + ' cadena. Puede agregar sinónimos una '
                                     + 'vez creada la entrada')
            self.__terminos = terminos

        @property
        def definicion(self):
            return self.__definicion

        @definicion.setter
        def definicion(self, definicion):
            self.__glosario.verificar_finalizado()
            if definicion is None:
                definicion = ''
            if type(definicion) is not str:
                raise AttributeError('La definición de una entrada del glosario'
                                     + ' debe ser una cadena')
            self.__definicion = definicion

        @definicion.deleter
        def definicion(self):
            del self.__definicion

        def verificar_completo(self):
            return self.__terminos and self.__definicion

        def _agregar_sinonimo(self, sinonimo):
            self.__glosario.verificar_finalizado()
            Glosario._Entrada.__valida_sinonimo(sinonimo)
            self.__terminos.add(sinonimo)

        def _suprimir_sinonimo(self, sinonimo):
            self.__glosario.verificar_finalizado()
            Glosario._Entrada.__valida_sinonimo(sinonimo)
            self.__terminos.remove(sinonimo)

        def __contains__(self, termino):
            return termino in self.__terminos

        def __str__(self):
            return 'Glosario._Entrada(terminos={},definicion={})'.format(
                self.__terminos, self.__definicion)

        def __eq__(self, otro):
            if type(otro) is not Glosario._Entrada:
                return False
            return self.__terminos == otro.__terminos and\
                self.__definicion == otro.__definicion

        def __gt__(self, otro):
            termino_mayor = None
            for termino in self.terminos:
                if termino_mayor is None or termino_mayor < termino:
                    termino_mayor = termino
            if type(otro) is str:
                return termino_mayor > otro
            if type(otro) is Glosario._Entrada:
                otro_mayor = None
                for termino in otro.terminos:
                    if otro_mayor is None or otro_mayor < termino:
                        otro_mayor = termino
                return termino_mayor > otro_mayor
            else:
                raise AttributeError('Un término del glosario únicamente puede '
                                     + 'ser comparado contra otro término o '
                                     + 'una cadena')

        def __hash__(self):
            return hash(frozenset(self.__terminos))

        def __formatea_termino(termino):
            return set([termino]) if type(termino) is str else None

        def __valida_sinonimo(sinonimo):
            if not sinonimo:
                raise AttributeError('El sinónimo no puede ser vacío')
            if type(sinonimo) is not str:
                raise AttributeError('El sinónimo debe ser una cadena')


class Colocacion:

    def __init__(self, ngrama, separador='_'):
        if type(ngrama) is not list:
            if type(ngrama) is not str:
                raise AttributeError('El n-grama con el que se forma una '
                                     + 'colocación debe ser una cadena de '
                                     + 'tokens separados por el separador '
                                     + 'dado; o bien, una lista de cadenas')
            ngrama = ngrama.split(separador)
        for term in ngrama:
            if type(term) is not str:
                raise AttributeError('El n-grama con el que se forma una '
                                     + 'colocación debe ser una cadena de '
                                     + 'tokens separados por el separador '
                                     + 'dado; o bien, una lista de cadenas')
        self.__separador = separador
        self.__formato = separador
        self.__terminos = copy.deepcopy(ngrama)

    @property
    def formato(self):
        return self.__formato

    @formato.setter
    def formato(self, nuevo_formato):
        self.__formato = nuevo_formato

    def __contains__(self, termino):
        if type(termino) is not str:
            return False
        for term in self.__terminos:
            if termino == term:
                return True
        return False

    def __eq__(self, otro):
        if type(otro) is str:
            return otro in self.__terminos
        if type(otro) is Colocacion:
            return self.__terminos == otro.__terminos
        return False

    def __str__(self):
        return self.__formato.join(self.__terminos)

    def __repr__(self):
        return 'Colocacion(terminos=' + str(self.__terminos) + ',separador='\
            + self.__separador + ')'

    def __iter__(self):
        self.__i = 0
        return self

    def __next__(self):
        if self.__i < len(self.__terminos):
            term = self.__terminos[self.__i]
            self.__i += 1
            return term
        raise StopIteration()

    def __hash__(self):
        return hash(tuple(self.__terminos))

    def colocaciones_con_terminos(colocaciones, termino):
        if type(termino) is not str:
            raise AttributeError('El término a buscar debe ser una cadena')
        colocaciones = set(colocaciones)
        return [colocacion for colocacion in colocaciones
                if termino in colocacion]
