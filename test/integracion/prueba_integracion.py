# This file is part of Requex.

# Requex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Requex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Requex.  If not, see <https://www.gnu.org/licenses/>.

from os import sep, remove
from requex import Requex
from requex import MineroRequerimientos

import logging
logger = logging.getLogger(__name__)


def prueba_extraccion_requerimientos():
    try:
        remove(MineroRequerimientos.archivo_modelo_ngramas)
        remove(MineroRequerimientos.archivo_modelo_diccionario)
        remove(MineroRequerimientos.archivo_mod_vect_sim)
        remove(MineroRequerimientos.archivo_mod_vect_rel)
        remove(MineroRequerimientos.archivo_modelo_sustantivos_clave)
        remove(MineroRequerimientos.archivo_modelo_verbos_clave)
        remove(MineroRequerimientos.archivo_modelo_perifrasis_verbal)
    except FileNotFoundError:
        pass
    requex = Requex('Proyecto de prueba')
    corpus_prueba = requex._Requex__cargar_corpus(sep.join(['res', 'text']))
    textos_corpus = []
    for texto in corpus_prueba.values():
        textos_corpus.append(texto)
    minero = MineroRequerimientos()
    minero.entrenar(textos_corpus)
    for archivo, doc in corpus_prueba.items():
        requerimientos, glosario = minero.extraer_requerimientos([doc])
        logger.debug(requerimientos)
        logger.debug(glosario)
        if requerimientos and glosario:
            break
        else:
            logger.debug('Pobre desempeño de extracción en el archivo: {}'
                         .format(archivo))
    assert requerimientos
    assert glosario
