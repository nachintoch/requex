# This file is part of Requex.

# Requex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Requex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Requex.  If not, see <https://www.gnu.org/licenses/>.

from collections import Counter
from requex.modelos import Colocacion


def prueba_sinonimos():
    colocacion_prueba = Colocacion('Prueba')
    colocacion_verificacion = Colocacion('Prueba_Verificacion')
    assert colocacion_prueba != colocacion_verificacion
    assert 'Prueba' in colocacion_verificacion
    colocacion_comprobacion = Colocacion(['Verificacion', 'Comprobacion'])
    assert colocacion_prueba != colocacion_comprobacion
    assert 'Verificacion' in colocacion_comprobacion


def prueba_lista_colocaciones():
    colocaciones = []
    colocaciones.append(Colocacion('Prueba_Verificacion'))
    colocaciones.append(Colocacion('Inteligencia_Artificial'))
    colocaciones.append(Colocacion('Computadora_Personal'))
    assert 'Artificial' in colocaciones
    assert 'Venado' not in colocaciones


def prueba_colocaciones_con_terminos():
    colocaciones = []
    colocaciones.append(Colocacion('Prueba_Verificacion'))
    colocaciones.append(Colocacion('Inteligencia_Artificial'))
    colocaciones.append(Colocacion('Computadora_Personal'))
    colocaciones.append(Colocacion('Ciudad_Universitaria'))
    colocaciones.append(Colocacion('Nada_Personal'))
    colocaciones_con_termino =\
        Colocacion.colocaciones_con_terminos(colocaciones, 'Personal')
    assert Counter(colocaciones_con_termino) ==\
        Counter([colocaciones[2], colocaciones[4]])


def prueba_str():
    colocacion_str = Colocacion('Prueba')
    colocacion_list = Colocacion('Inteligencia_Artificial')
    assert 'Prueba' == str(colocacion_str)
    assert str('_'.join(['Inteligencia', 'Artificial'])) ==\
        str(colocacion_list)


def prueba_iter():
    colocacion = Colocacion('Inteligencia_Artificial_Computadora_Personal')
    for termino in colocacion:
        assert termino in ['Inteligencia', 'Artificial', 'Computadora',
                           'Personal']
