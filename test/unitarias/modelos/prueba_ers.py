# This file is part of Requex.

# Requex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Requex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Requex.  If not, see <https://www.gnu.org/licenses/>.

from requex.modelos import EspecificacionRequerimientosSoftware
from prueba_requerimento import crear_requerimiento
from prueba_requerimento import completar_requerimiento
import pytest


def crear_ers_prueba():
    ers = EspecificacionRequerimientosSoftware()
    ers.titulo = 'Documento de prueba'
    ers.alcances = 'Este documento es una prueba'
    ers.version = '0.0'
    return ers


def prueba_agregar_requerimiento():
    ers = crear_ers_prueba()
    lon_requerimientos = len(ers.requerimientos)
    assert lon_requerimientos == 0, 'Los requerimientos deben inicializarse '\
        + 'como una lista vacia'
    requerimiento = crear_requerimiento()
    no_requerimiento = ers.agregar_requerimiento(requerimiento)
    assert len(ers.requerimientos) == lon_requerimientos+1
    assert ers.requerimientos[no_requerimiento] == requerimiento


def prueba_suprimir_requerimiento():
    ers = crear_ers_prueba()
    no_requerimiento = ers.agregar_requerimiento(crear_requerimiento())
    lon_requerimientos = len(ers.requerimientos)
    assert ers.suprimir_requerimiento(no_requerimiento), \
        'suprimir_requerimiento debe devolver un booleano que indique si la '\
        + 'operación fue exitosa'
    assert len(ers.requerimientos) == lon_requerimientos-1


def prueba_agregar_referencia():
    ers = crear_ers_prueba()
    lon_referencias = len(ers.referencias)
    assert lon_referencias == 0, 'Las referencias deben inicializarse como '\
        + 'una lista vacia'
    referencia = 'Yo mismo'
    no_referencia = ers.agregar_referencia(referencia)
    assert len(ers.referencias) == lon_referencias+1
    assert ers.referencias[no_referencia] == referencia


def prueba_suprimir_referencia():
    ers = crear_ers_prueba()
    no_referencia = ers.agregar_referencia('Yo mismo')
    lon_referencias = len(ers.referencias)
    assert ers.suprimir_referencia(no_referencia), 'suprimir_referencia debe '\
        + 'devolver un booleano que indique si la operación fue exitosa'
    assert len(ers.referencias) == lon_referencias-1


def prueba_agregar_responsable_emision():
    ers = crear_ers_prueba()
    lon_responsables = len(ers.responsables_emision)
    assert lon_responsables == 0, 'Los responsables de emisión deben '\
        + 'inicializarse como una lista vacia'
    ers.agregar_responsable_emision('John Doe')
    assert len(ers.responsables_emision) == lon_responsables+1


def prueba_suprimir_responsable_emision():
    ers = crear_ers_prueba()
    no_responsable = ers.agregar_responsable_emision('John Doe')
    lon_responsables = len(ers.responsables_emision)
    assert ers.suprimir_responsable_emision(no_responsable)
    assert len(ers.responsables_emision) == lon_responsables-1


def prueba_verificar_completo():
    ers = crear_ers_prueba()
    estados_validos = \
        EspecificacionRequerimientosSoftware.obtener_estados_validos()
    estado = ers.verificar_completo()
    assert estados_validos[estado] == 'No revisado'
    ers.agregar_responsable_emision('John Doe')
    requerimiento = crear_requerimiento()
    no_requerimiento = ers.agregar_requerimiento(requerimiento)
    ers.agregar_referencia('Yo mismo')
    estado = ers.verificar_completo()
    assert estados_validos[estado] == 'Revisión incompleta'
    no_entrada = ers.glosario.agregar_entrada('Prueba', 'Ensayo o experimento '
                                              + 'que se hace de algo, para '
                                              + 'saber cómo resultará en su '
                                              + 'forma definitiva')
    completar_requerimiento(requerimiento)
    estado = ers.verificar_completo()
    assert estados_validos[estado] == 'Revisado'
    ers.suprimir_requerimiento(no_requerimiento)
    estado = ers.verificar_completo()
    assert estados_validos[estado] == 'Revisión incompleta'
    ers.agregar_requerimiento(requerimiento)
    estado = ers.verificar_completo()
    assert estados_validos[estado] == 'Revisado'
    ers.glosario.editar_definicion(no_entrada, '')
    estado = ers.verificar_completo()
    assert estados_validos[estado] == 'Revisión incompleta'


def prueba_verificar_finalizado():
    ers = crear_ers_prueba()
    ers.finalizar()
    with pytest.raises(Exception):
        ers.responsables_emision.agregar_responsable_emision('John Doe')


def prueba_eq():
    una_ers = crear_ers_prueba()
    assert una_ers == una_ers
    otra_ers = crear_ers_prueba()
    assert una_ers == otra_ers
    assert hash(una_ers) == hash(otra_ers)
    otra_ers.agregar_responsable_emision('John Doe')
    assert una_ers != otra_ers
    assert hash(una_ers) != hash(otra_ers)
