# This file is part of Requex.

# Requex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Requex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Requex.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from requex.modelos import Glosario


def crear_registro(glosario):
    return glosario.agregar_entrada('Prueba', 'Ensayo o experimento que se'
                                    + ' hace de algo, para saber cómo '
                                    + 'resultará en su forma definitiva')


def prueba_agregar_entrada():
    glosario = Glosario()
    lon_glosario = len(glosario)
    assert lon_glosario == 0, 'El glosario debe ser inicializado como una '\
        + 'lista vacia'
    id_esperado = Glosario._Glosario__generador_id_entradas
    no_entrada = crear_registro(glosario)
    assert glosario.obtener_entrada(id_esperado) ==\
        glosario.obtener_entrada(no_entrada)
    assert len(glosario) == lon_glosario + 1


def prueba_suprimir_entrada():
    glosario = Glosario()
    no_entrada = crear_registro(glosario)
    lon_glosario = len(glosario)
    assert glosario.suprimir_entrada(no_entrada), \
        'suprimir_entrada_glosario debe devolver un booleano que indique si '\
        + 'la operación fue exitosa o no'
    assert len(glosario) == lon_glosario - 1


def prueba_agregar_sinonimo():
    glosario = Glosario()
    no_entrada = crear_registro(glosario)
    no_terminos = len(glosario.obtener_entrada(no_entrada).terminos)
    assert no_terminos == 1
    glosario.agregar_sinonimo(no_entrada, 'Probar')
    assert len(glosario.obtener_entrada(no_entrada).terminos) == no_terminos + 1


def prueba_suprimir_sinonimo():
    glosario = Glosario()
    no_entrada = crear_registro(glosario)
    no_terminos = len(glosario.obtener_entrada(no_entrada).terminos)
    glosario.agregar_sinonimo(no_entrada, 'Probar')
    assert glosario.suprimir_sinonimo(no_entrada, 'Probar')
    assert len(glosario.obtener_entrada(no_entrada).terminos) == no_terminos


def prueba_editar_definicion():
    glosario = Glosario()
    no_entrada = crear_registro(glosario)
    nueva_def = 'Operación matemática que se ejecuta para comprobar que otra '\
        + 'ya hecha es correcta.'
    assert glosario.editar_definicion(no_entrada, nueva_def)
    assert glosario.obtener_entrada(no_entrada).definicion == nueva_def


def prueba_editar_entrada():
    glosario = Glosario()
    no_entrada = crear_registro(glosario)
    glosario.obtener_entrada(no_entrada)
    terminos = 'Probar'
    definicion = 'Operación matemática que se ejecuta para comprobar que otra '\
        + 'ya hecha es correcta.'
    assert glosario.editar_entrada(no_entrada, terminos, definicion)
    entrada_editada = glosario.obtener_entrada(no_entrada)
    assert entrada_editada.terminos == set([terminos])
    assert entrada_editada.definicion == definicion


def prueba_obtener_id_entrada():
    glosario = Glosario()
    no_entrada = crear_registro(glosario)
    assert no_entrada == glosario.obtener_id_entrada('Prueba')


def prueba_verificar_completo():
    glosario = Glosario()
    assert not glosario.verificar_completo()
    no_entrada = glosario.agregar_entrada('Prueba')
    assert not glosario.verificar_completo()
    glosario.editar_definicion(no_entrada, 'Ensayo o experimento que se  hace '
                               + 'de algo, para saber cómo resultará en su '
                               + 'forma definitiva')
    assert glosario.verificar_completo()
    glosario.agregar_entrada('Ejemplo')
    assert not glosario.verificar_completo()


def prueba_finalizar():
    glosario = Glosario()
    glosario.finalizar()
    with pytest.raises(Exception):
        glosario.agregar_entrada('Entrada')


def prueba_pertenencia():
    glosario = Glosario()
    crear_registro(glosario)
    assert 'Prueba' in glosario
    assert 'Ejemplo' not in glosario


def prueba_vacio():
    glosario = Glosario()
    assert not glosario
    no_entrada = crear_registro(glosario)
    assert glosario
    glosario.suprimir_entrada(no_entrada)
    assert not glosario


def prueba_comparacion():
    glosario = Glosario()
    prueba_id = crear_registro(glosario)
    otro_id = glosario.agregar_entrada('Zotero')
    glosario.agregar_sinonimo(otro_id, 'Manejador de referencias')
    prueba = glosario.obtener_entrada(prueba_id)
    otro = glosario.obtener_entrada(otro_id)
    assert otro > prueba
