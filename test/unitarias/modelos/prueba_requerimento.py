# This file is part of Requex.

# Requex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Requex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Requex.  If not, see <https://www.gnu.org/licenses/>.

from requex.modelos import Requerimiento


def crear_requerimiento():
    return Requerimiento(-1, 'Probar la implementación',
                         'Probar la implementación de Requex', 'Identificado')


def completar_requerimiento(requerimiento):
    requerimiento.prioridad = 10
    requerimiento.volatilidad = 10
    requerimiento.tipo = 'Calidad'
    requerimiento.riesgo = 'Coordinar la prueba con usuarios puede requerir '\
        + 'un par de semanas más allá de la fecha de corte para el objetivo 1'
    requerimiento.clausula_organizacion = ('Jerarquía funcional',
                                           'Primer nivel')


def prueba_verificar_completo():
    requerimiento = crear_requerimiento()
    assert not requerimiento.verificar_completo()
    completar_requerimiento(requerimiento)
    assert requerimiento.verificar_completo()


def prueba_eq():
    un_requerimiento = crear_requerimiento()
    assert un_requerimiento == un_requerimiento
    otro_requerimiento = crear_requerimiento()
    assert un_requerimiento == otro_requerimiento
    assert hash(un_requerimiento) == hash(otro_requerimiento)
    un_requerimiento.prioridad = 10
    assert un_requerimiento != otro_requerimiento
    assert hash(un_requerimiento) != hash(otro_requerimiento)


def prueba_estados_validos():
    estados_swebok, estados_essence = Requerimiento.obtener_estados_validos()
    assert estados_swebok == ['En progreso', 'En revisión', 'Suspendido',
                              'Terminado']
    assert estados_essence == ['Identificado', 'Descrito', 'Implementado',
                               'Verificado']


def prueba_tipos_validos():
    tipos = Requerimiento.obtener_tipos_validos()
    assert tipos == ['Funcional', 'Interfaz', 'Calidad', 'No funcional',
                     'Usabilidad', 'Factor humano']


def prueba_clausulas_organizacion_validas():
    organizacion_tipos, organizacion_funcion = \
        Requerimiento.obtener_clausulas_organizacion_validas()
    assert organizacion_tipos == Requerimiento.obtener_tipos_validos()
    assert organizacion_funcion == ['Modo del sistema', 'Clase de usuario',
                                    'Objeto', 'Característica', 'Estímulo',
                                    'Respuesta', 'Jerarquía funcional']
