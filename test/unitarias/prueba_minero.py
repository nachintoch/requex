# This file is part of Requex.

# Requex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Requex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Requex.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from os import path, remove
from numpy import ndarray
from spacy.tokens import Span, Token
from requex import MineroRequerimientos
from requex.modelos import Requerimiento


def prueba_entrenamiento():
    try:
        remove(MineroRequerimientos.archivo_modelo_ngramas)
        remove(MineroRequerimientos.archivo_modelo_diccionario)
        remove(MineroRequerimientos.archivo_mod_vect_sim)
        remove(MineroRequerimientos.archivo_mod_vect_rel)
        remove(MineroRequerimientos.archivo_modelo_sustantivos_clave)
        remove(MineroRequerimientos.archivo_modelo_verbos_clave)
        remove(MineroRequerimientos.archivo_modelo_perifrasis_verbal)
    except FileNotFoundError:
        pass
    minero = MineroRequerimientos()
    texto_prueba = 'Este es un texto de prueba'
    minero.entrenar([texto_prueba], congelar_modelos=False, freq_min=1)
    assert path.exists(MineroRequerimientos.archivo_modelo_ngramas)
    assert path.exists(MineroRequerimientos.archivo_modelo_diccionario)
    assert path.exists(MineroRequerimientos.archivo_mod_vect_sim)
    assert path.exists(MineroRequerimientos.archivo_mod_vect_rel)
    assert path.exists(MineroRequerimientos.archivo_modelo_sustantivos_clave)
    assert path.exists(MineroRequerimientos.archivo_modelo_verbos_clave)
    assert path.exists(MineroRequerimientos.archivo_modelo_perifrasis_verbal)
    assert len(minero._MineroRequerimientos__sustantivos_clave) ==\
        len(MineroRequerimientos.obtener_sustantivos_clave())
    assert len(minero._MineroRequerimientos__verbos_clave) ==\
        len(MineroRequerimientos.obtener_verbos_clave())
    assert len(minero._MineroRequerimientos__verbos_perifrasis) == \
        len(MineroRequerimientos.obtener_verbos_perifrasis())
    texto_requerimiento_prueba = 'Estructura de la Especificación de '\
                                 + 'Requerimientos a producir'
    minero.entrenar([texto_requerimiento_prueba], congelar_modelos=True,
                    freq_min=1)
    with pytest.raises(Exception):
        minero.entrenar(['No pasarás'], freq_min=1)


def prueba_preprocesamiento():
    minero = MineroRequerimientos()
    oraciones_etiquetadas =\
        minero._MineroRequerimientos__preprocesar('Este es un texto de prueba')
    assert hasattr(oraciones_etiquetadas, '__iter__')
    for oracion in oraciones_etiquetadas:
        assert type(oracion) is Span
        for token in oracion:
            assert type(token) is Token
        oracion = list(oracion)
        assert not oracion[2].is_stop
        assert oracion[4].is_stop


def prueba_vectorizar_similitud():
    minero = MineroRequerimientos()
    oraciones_etiquetadas =\
        minero._MineroRequerimientos__preprocesar('Este es un texto de prueba')
    oraciones_filtradas =\
        minero._MineroRequerimientos__filtrar_palabras_vacias(
            oraciones_etiquetadas)
    vectores = minero._MineroRequerimientos__vectorizar_similitud(
        oraciones_filtradas)
    assert type(vectores) is dict
    for id, vector in vectores.items():
        assert type(id) is str
        assert type(vector) is ndarray


def prueba_vectorizar_relevancia():
    minero = MineroRequerimientos()
    oraciones_etiquetadas =\
        minero._MineroRequerimientos__preprocesar('Este es un texto de prueba')
    oraciones_filtradas =\
        minero._MineroRequerimientos__filtrar_palabras_vacias(
            oraciones_etiquetadas)
    vectores = minero._MineroRequerimientos__vectorizar_relevancia(
        oraciones_filtradas)
    assert type(vectores) is dict
    for id, tfidf in vectores.items():
        assert type(id) is str
        assert type(tfidf) is float


def prueba_colocaciones():
    minero = MineroRequerimientos()
    oraciones_etiquetadas =\
        minero._MineroRequerimientos__preprocesar('Este es un texto de prueba')
    oraciones_filtradas =\
        minero._MineroRequerimientos__filtrar_palabras_vacias(
            oraciones_etiquetadas)
    vectores = minero._MineroRequerimientos__vectorizar_similitud(
        oraciones_filtradas)
    colocaciones = minero._MineroRequerimientos__detectar_colocaciones(
        oraciones_filtradas, vectores)
    assert type(colocaciones) is set
    for colocacion in colocaciones:
        assert type(colocacion) is str


def prueba_minar_requerimientos():
    minero = MineroRequerimientos(relevancia_min=20)
    oraciones_etiquetadas =\
        minero._MineroRequerimientos__preprocesar('Estructura de la '
                                                  + 'Especificación de '
                                                  + 'Requerimientos a '
                                                  + 'producir')
    oraciones_filtradas =\
        minero._MineroRequerimientos__filtrar_palabras_vacias(
            oraciones_etiquetadas)
    assert len(oraciones_etiquetadas) == len(oraciones_filtradas)
    vectores_similitud = minero._MineroRequerimientos__vectorizar_similitud(
        oraciones_filtradas)
    vectores_relevancia = minero._MineroRequerimientos__vectorizar_relevancia(
        oraciones_filtradas)
    colocaciones = minero._MineroRequerimientos__detectar_colocaciones(
        oraciones_filtradas, vectores_similitud)
    requerimientos, glosario = minero._MineroRequerimientos__minar(
        oraciones_etiquetadas, colocaciones, vectores_relevancia)
    assert type(requerimientos) is set
    for requerimiento in requerimientos:
        assert type(requerimiento) is Requerimiento
        assert requerimiento.descripcion.startswith(requerimiento.titulo)
    assert type(glosario) is set
