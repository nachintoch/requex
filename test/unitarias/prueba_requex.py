# This file is part of Requex.

# Requex is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Requex is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with Requex.  If not, see <https://www.gnu.org/licenses/>.

from os import sep, path
from requex import Requex


def prueba_cargar_corpus():
    requex = Requex('Proyecto de prueba')
    corpus = requex._Requex__cargar_corpus(sep.join(['res', 'text']))
    assert type(corpus) is dict
    for archivo, doc in corpus.items():
        assert type(archivo) is str
        assert path.exists(archivo)
        assert type(doc) is str


def prueba_papelera_requerimientos():
    pass


def prueba_eleccion_formato_salida():
    pass


def prueba_entrada_no_interactiva():
    pass
