#!/bin/bash

# Reemplaza los espacios en blanco en los archivos encontrados por _
# El propósito es permitir la ejecución de otros scripts que pueden tener
# problemas interpretando rutas con espacios en blanco.

function ayuda {
  printf "Reemplaza los espacios en blanco en los archivos encontrados por _\n\
Uso: elimina_espacios_ruta.sh [opciones] [fuente]\n\
fuente - Archivo o directorio donde se buscarán archivos y directorios con \
espacios en blanco en su nombre para reemplazarlos por _\n\
opciones:\n\
 -r, --recursive\tIndica que se deben buscar archivos y directorios con \
 espacios en blanco de forma recursiva. Por defecto, solo se manupulan los \
 archivos y directorios en la fuente dada.\n\
 -h, --help\t\tMuestra este mensaje de ayuda y termina.\n\
 \n\
 Por ejemplo: elimina_espacios_ruta.sh -r dir\n"
}

function elimina_espacios_ruta {
  find $1 $2 -name '* *' -type $3 | while read f; do
    nombre_nuevo=`echo "$f" | sed -e 's/ /_/g'`
    if [ "$nombre_nuevo" = "$f" ]; then
      continue
    fi
    if [ -e "$nombre_nuevo" ]; then
      echo "Se omite $f pues $nombre_nuevo ya existe"
    else
      mv "$f" "$nombre_nuevo"
    fi
  done
}

RECURSIVO="-maxdepth 1"
POSICIONAL=()
while [[ $# -gt 0 ]]; do
  arg=$1
  case $arg in
    -r|--recursive)
    RECURSIVO=""
    shift
    ;;
    -h|--help)
    AYUDA="$2"
    shift
    ;;
    *)
    POSICIONAL+=("$1")
    shift
    ;;
  esac
done
set -- "${POSICIONAL[@]}"

if [[ -z "$1" ]]; then
  echo "Error. Debes proveer un archivo o directorio a procesar."
  ayuda
  exit 1
fi

if [[ -n "$AYUDA" ]]; then
  ayuda
  exit 0
fi

elimina_espacios_ruta $1 "$RECURSIVO" d
elimina_espacios_ruta $1 "$RECURSIVO" f
exit 0
