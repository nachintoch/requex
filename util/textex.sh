#!/bin/bash

# Extrae el texto contenido en las fuentes de datos y las deposita en un
# directorio alternativo o en la salida estándar

# Este script depende de las siguientes herramientas:
# pdftotext - http://www.xpdfreader.com/pdftotext-man.html
# catdoc - http://www.wagner.pp.ru/~vitus/software/catdoc/
# docx2txt - http://docx2txt.sourceforge.net/

function ayuda {
  printf "Extrae texto plano de archivos PDF, DOC y DOCX\n\
Uso: textex.sh [opciones] [fuente]\n\
fuente - Archivo o directorio de donde se buscarán archivos soportados para\
extraer sus textos. Los archivos en la ruta de la fuente no pueden tener\
espacios en blanco.\n\
opciones:
 -r, --recursive\tIndica que se debe extraer el texto de todos los archivos\
 anidados en el directorio fuente indicado. Por defecto, solo se leen los\
 archivos dentro de la fuente dada.\n\
 -o, --output\t\tIndica el directorio de salida para el resultado, que llevará\
 el nombre de (los) archivo(s) procesado(s). Si no se especifica, la salida se\
 escribe en la salida estándar.\n\
 -h, --help\t\tMuestra este mensaje de ayuda y termina.\n\
\n\
Por ejemplo: textex.sh -r -o dir/destino dir/archivos\n"
}

# Extrae el texto de los archivos fuente que se encuentren en la fuente
# Recibe dos parámetos:
# $1 es la ruta que indica el archivo en la "fuente" que recibe el "main"
# $2 es la profundidad de exploración fija en 1 o una cadena vacía. Esto
# desencadena la exploración recursiva o no de la fuente del usuario
function extraer_texto {
  find $1 $2 -name '*.pdf' -or -name '*.doc' -or -name '*.docx' | while read f
  do
    case ${f: -4} in
      ".pdf")
      resultado=`pdftotext $f -`
      f=${f%".pdf"}
      ;;
      ".doc")
      resultado=`catdoc $f`
      f=${f%".doc"}
      ;;
      "docx")
      resultado=`docx2txt $f -`
      f=${f%".docx"}
      ;;
    esac
    if [[ -n "$3" ]]; then
      archivo=${f#"$1"}
      directorio=$(dirname "${archivo}")
      directorio="${3}${directorio}"
      mkdir -p "$directorio"
      echo -n "$resultado" > "${3}${archivo}.txt"
    else
      echo -n "$resultado"
    fi
  done
}

RECURSIVO="-maxdepth 1"

POSICIONAL=()
while [[ $# -gt 0 ]]; do
  arg=$1
  case $arg in
    -r|--recursive)
    RECURSIVO=""
    shift
    #shift this affects the argument, bit -r is not expected to have any
    ;;
    -o|--output)
    SALIDA="$2"
    shift
    shift
    ;;
    -h|--help)
    AYUDA="$2"
    shift
    ;;
    *)
    POSICIONAL+=("$1")
    shift
    ;;
  esac
done
set -- "${POSICIONAL[@]}"

if [[ -z "$1" ]]; then
  echo "Error. Debes proveer un archivo o directorio a leer."
  ayuda
  exit 1
fi

if [[ -n "$AYUDA" ]]; then
  ayuda
  exit 0
fi

if [[ -n "$SALIDA" ]]; then
  SALIDA=`echo $SALIDA | sed 's![^/]$!&/!'`
fi

extraer_texto $1 "$RECURSIVO" $SALIDA
exit 0
